<?php

namespace App\Enum;

enum BillStatusEnum: string
{
    case PENDING = '0';
    case VALIDATED = '1';
}
