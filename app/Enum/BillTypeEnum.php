<?php

namespace App\Enum;

enum BillTypeEnum: string
{
    case CLIENT_PAYMENT = '0';
    case OSP_PAYMENT = '1';
}
