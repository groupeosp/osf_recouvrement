<?php

namespace App\Enum;

enum FeeTypeEnum: string
{
    case ENGAGED_FEE = '1';
    case COMPLEMENTARY_FEE = '2';
}
