<?php

namespace App\Enum;

enum FileTypeEnum: string
{
    case FILES = '1';
    case BILLS = '2';
}
