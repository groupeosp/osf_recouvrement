<?php

namespace App\Enum;

enum PaiementTypeEnum: string
{
    case CHEQUE = '0';
    case CB = '1';
    case VIREMENT = '2';
}
