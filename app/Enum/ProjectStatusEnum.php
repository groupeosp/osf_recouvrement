<?php

namespace App\Enum;

enum ProjectStatusEnum: string
{
    case OPEN = '0';
    case CLOSE = '1';
    case BILLING = '2';
}
