<?php

namespace App\Enum;

enum UserRoleEnum: string
{
    case ADMIN = 'Admin';
    case LEAD_ACCOUNTANT = 'Chef Comptable';
    case ACCOUNTANT = 'Comptable';
    case USER = 'Utilisateur';
}
