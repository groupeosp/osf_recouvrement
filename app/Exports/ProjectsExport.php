<?php

namespace App\Exports;

use App\Models\Project;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\Cookie;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ProjectsExport implements FromCollection, Responsable, WithHeadings, ShouldAutoSize, WithMapping, WithColumnFormatting
{
    use Exportable;

    private $fileName = '';
    /**
     * Optional Writer Type
     */
    private $writerType = Excel::XLSX;
    /**
     * Optional headers
     */
    private $headers = [
        'Content-Type' => 'text/csv',
    ];

    public function headings(): array
    {
        return [
            'N° DOSSIER',
            'NOM DÉBITEUR',
            'MONTANT CRÉANCE',
            'NB FACTURES',
            'PHASE',
            'ÉTAPE EN COURS',
            'ÉTAPE SUIVANTE',
            'SOLDE RESTANT DÛ',
            'DATE SOLDE RESTANT DÛ',
            'MONTANT DES FRAIS ENGAGÉS',
            'DATE DU DERNIER RECOUVREMENT',
            'MONTANT DU DERNIER RECOUVREMENT',
            'DATE CREATION'
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'H' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'I' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'J' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'K' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'L' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
            'M' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function __construct($date)
    {
        $this->fileName = "Bilan-dossiers_$date.xlsx";
        $this->date = $date;
    }

    public function map($project): array
    {

        return [
            "N°$project->id",
            $project->debtor->last_name,
            $project->amount_ttc / 100,
            $project->bill_number,
            $project->currentPhase,
            $project->currentStep ? $project->currentStep->name : 'Aucune étape en cours',
            $project->nextGroupStep ? $project->nextGroupStep->name : 'Aucune étape à venir',
            ($project->amount_ttc - $project->billSum) / 100,
            now(),
            $project->totalEngagedFeesTTC / 100,
            $project->lastBillingDate,
            $project->lastBillingAmount / 100,
            $project->created_at
        ];
    }

    public function collection()
    {
        return $this->getActionItems();
    }

    public function getActionItems()
    {
        if (Cookie::get('client_id')) {
            $projects = Project::where('client_id', Cookie::get('client_id'))->where('created_at', '>=', date('Y-m-d', strtotime($this->date)))->where('created_at', '<=', date('Y-m-t', strtotime($this->date)))
                ->with(['debtor'])->get();
        } else {
            abort(500, 'Client id not found');
        }


        return $projects;
    }
}
