<?php

namespace App\Filters\Projects;

use Closure;

class Client
{
    public function handle($request, Closure $next)
    {
        if (!isset($request->client_id)) {
            return $next($request);
        }

        return $next($request)->where('client_id', $request->client_id);
    }
}
