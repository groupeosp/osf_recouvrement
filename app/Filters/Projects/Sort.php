<?php

namespace App\Filters\Projects;

use Closure;

class Sort
{
    public function handle($request, Closure $next)
    {
        if (!isset($request->order)) {
            return $next($request);
        }

        return $next($request)->orderBy($request->order['by'], $request->order['type']);
    }
}
