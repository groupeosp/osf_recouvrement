<?php

namespace App\Filters\Projects;

use Closure;

class Status
{
    public function handle($request, Closure $next)
    {
        if (!isset($request->is_closed)) {
            return $next($request);
        }

        return $next($request)->where('status', $request->is_closed);
    }
}
