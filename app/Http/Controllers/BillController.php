<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Enum\BillStatusEnum;
use App\Enum\BillTypeEnum;
use App\Enum\PaiementTypeEnum;
use Illuminate\Http\Request;
use App\Http\Requests\StoreBillRequest;
use App\Http\Requests\UpdateBillRequest;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $bills = Bill::when($request->has('project_id'), function ($query) use ($request) {
            $query->where('project_id', $request->get('project_id'));
        })
            ->when($request->has('creators'), function ($query) {
                return $query->with(['createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name']);
            })->get();


        return response()->json($bills);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        /*  dd($request->type == BillTypeEnum::CLIENT_PAYMENT->value); */
        $bill = new Bill();
        $bill->project_id = $request->project_id;
        $bill->billing_date = $request->form['date'];
        $bill->billing_amount_ttc = $request->form['amount_ttc'] * 100;

        if ($request->form['type'] == BillTypeEnum::CLIENT_PAYMENT->value) {
            $bill->type = BillTypeEnum::CLIENT_PAYMENT->value;
            $bill->paiement_date = now();
            $bill->status = BillStatusEnum::VALIDATED->value;
        } else {
            $bill->type = BillTypeEnum::OSP_PAYMENT->value;
            $bill->status = BillStatusEnum::PENDING->value;
        }
        switch ($request->form['paiement_type']) {
            case PaiementTypeEnum::CHEQUE->value:
                $bill->paiement_type = PaiementTypeEnum::CHEQUE->value;
                break;
            case PaiementTypeEnum::CB->value:
                $bill->paiement_type = PaiementTypeEnum::CB->value;
                break;
            case PaiementTypeEnum::VIREMENT->value:
                $bill->paiement_type = PaiementTypeEnum::VIREMENT->value;
                break;
            default:
                $bill->paiement_type = PaiementTypeEnum::VIREMENT->value;
                break;
        }
        $bill->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function show(Bill $bill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function edit(Bill $bill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBillRequest  $request
     * @param  \App\Models\Bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBillRequest $request, Bill $bill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Int $id
     * @return void
     */
    public function destroy(Int $id)
    {
        $bill = Bill::findOrFail($id)->delete();
        if ($bill) {
            return response()->json(['message' => 'Bill deleted']);
        } else {
            return abort(500, 'Bill not deleted');
        }
    }

    public function validation($bill_id, Request $request)
    {
        $bill = Bill::findOrFail($bill_id);
        $bill->status = BillStatusEnum::VALIDATED->value;
        $bill->paiement_date = $request->form;
        $bill->save();
    }
}
