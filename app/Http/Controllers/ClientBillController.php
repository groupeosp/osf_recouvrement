<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ClientBill;
use Illuminate\Http\Request;
use App\Enum\ProjectStatusEnum;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\FileRepositoryInterface;
use App\Http\Requests\StoreClientBillRequest;
use App\Http\Requests\UpdateClientBillRequest;
use App\Interfaces\ProjectRepositoryInterface;
use App\Interfaces\ClientBillRepositoryInterface;
use App\Interfaces\ProjectStepRepositoryInterface;

class ClientBillController extends Controller
{

    const CLOTURE_STEP_ID = 20;

    private ClientBillRepositoryInterface $clientBillRepository;
    private ProjectRepositoryInterface $projectRepository;
    private ProjectStepRepositoryInterface $projectStepRepository;
    private FileRepositoryInterface $fileRepository;

    public function __construct(FileRepositoryInterface $fileRepository, ClientBillRepositoryInterface $clientBillRepository, ProjectRepositoryInterface $projectRepository, ProjectStepRepositoryInterface $projectStepRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->clientBillRepository = $clientBillRepository;
        $this->projectStepRepository = $projectStepRepository;
        $this->fileRepository = $fileRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function show(int $client_bill_id)
    {
        $file = $this->clientBillRepository->showClientBill($client_bill_id);

        $path = 'app/clients/' . $file->client_id . '/bills/' . $file->hashname . '.pdf';

        return response()->file(storage_path($path), [
            'Content-Disposition' => 'inline; filename=contrat-de-mandat.pdf'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function edit(int $client_bill_id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClientBillRequest  $request
     * @param  \App\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientBillRequest $request, ClientBill $clientBill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClientBill  $clientBill
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $client_bill_id)
    {
        $client_file = $this->clientBillRepository->destroyClientBill($client_bill_id);

        return response()->json($client_file);
    }

    public function previewPDF(Request $request)
    {
        $pdf =  $this->clientBillRepository->generateClientBillPDF(true, $request->date_start, $request->date_end, $request->client_id, $request->date_ignore, explode(',', $request->array_id), null, null, false);
        return $pdf->stream();
    }

    public function exportPDF(Request $request)
    {
        //Test if cegid_id is set
        if (ClientBill::where('cegid_id', $request->cegid_id)->exists()) {
            abort(500, 'Le N° Cegid est deja utilisé pour une autre facture');
        }

        $name = "Recap_facture_" . date('dmY') . ".pdf";

        $pdf =  $this->clientBillRepository->generateClientBillPDF(false, $request->date_start, $request->date_end, $request->client_id, $request->date_ignore, explode(',', $request->array_id), null, $request->cegid_id, true);

        // Update the project to billed status then export the bill
        foreach (explode(',', $request->array_id) as $project_id) {
            $this->projectRepository->updateProjectStatus($project_id, ProjectStatusEnum::BILLING->value);
            $this->projectStepRepository->storeProjectStep($project_id, self::CLOTURE_STEP_ID, date('Y-m-d'));

            $project = Project::findOrFail($project_id);
            $this->fileRepository->storePDFFile($pdf['pdf']->output(), $name,  $request->client_id, $project_id, $project->currentProjectSteps->id);
        }

        /* return $pdf->stream(); */
        return response()->json($pdf['bill_id']);
    }
    /**
     * Download the specified resource.
     *
     * @param Int $contract_id
     * @return void
     */
    public function download(Int $client_bill_id)
    {
        $clientBill = $this->clientBillRepository->showClientBill($client_bill_id);

        $path = 'clients/' . $clientBill->client_id . '/bills/' . $clientBill->hashname . '.pdf';
        return Storage::download($path, "Facture-" . $clientBill->id . "_" . date('dmY') . ".pdf");
    }
}
