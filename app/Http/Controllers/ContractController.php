<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreContractRequest;
use App\Http\Requests\UpdateContractRequest;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        if (isset($request->contract) && is_file($request->contract)) {
            $file = $request->contract;
            // Prevent from '/' in the filename
            $hash_name = str_replace('/', '', Hash::make($file->getClientOriginalName()));
            $path = 'clients/' . $request->client_id . '/contracts';

            $path = Storage::putFileAs(
                $path,
                $file,
                $hash_name . '.' . $file->getClientOriginalExtension()
            );

            if ($path) {
                $contract = new Contract();
                $contract->client_id = $request->client_id;
                $contract->name = explode(".", $file->getClientOriginalName())[0];
                $contract->hashname = $hash_name;
                $contract->size = $file->getSize();
                $contract->extension = $file->getClientOriginalExtension();
                $contract->save();

                return response()->json($contract);
            }
            return abort(500, 'Storage has failed');
        }

        return abort(500, 'File not found');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function show(Int $contract_id)
    {
        $contract = Contract::select(['client_id', 'name', 'hashname', 'extension'])->findOrFail($contract_id);

        $path = 'app/clients/' . $contract->client_id . '/contracts/' . $contract->hashname . '.' . $contract->extension;
        return response()->file(storage_path($path), [
            'Content-Disposition' => 'inline; filename=contrat-de-mandat.pdf'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateContractRequest  $request
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContractRequest $request, Contract $contract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contract  $contract
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $contract_id)
    {
        $contract = Contract::findOrFail($contract_id)->delete();
        if ($contract) {
            return response()->json(['message' => 'Contract deleted']);
        } else {
            return abort(500, 'Contract not deleted');
        }
    }

    /**
     * Download the specified resource.
     *
     * @param Int $contract_id
     * @return void
     */
    public function download(Int $contract_id)
    {
        $contract = Contract::findOrFail($contract_id);
        $path = 'clients/' . $contract->client_id . '/contracts/' . $contract->hashname . '.' . $contract->extension;
        return Storage::download($path, 'Contrat_de_mandat.pdf');
    }
}
