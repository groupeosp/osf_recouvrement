<?php

namespace App\Http\Controllers;

use App\Models\Fee;
use Illuminate\Http\Request;
use App\Http\Requests\StoreFeeRequest;
use App\Http\Requests\UpdateFeeRequest;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fees = Fee::when($request->has('project_id'), function ($query) use ($request) {
            $query->where('project_id', $request->get('project_id'));
        })
            ->when($request->has('creators'), function ($query) {
                return $query->with(['createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name']);
            })->get();


        return response()->json($fees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*  $fee = Fee::create(); */
        $fee = new Fee($request->all()['form']);
        $fee->amount_ttc *= 100;
        $fee->amount_ht *= 100;
        $fee->save();

        return $fee ? response()->json($fee) :  abort(500, 'Fee not created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function show(Fee $fee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function edit(Fee $fee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFeeRequest  $request
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFeeRequest $request, Fee $fee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function destroy($fee_id)
    {
        $fee = Fee::findOrFail($fee_id)->delete();
        if ($fee) {
            return response()->json(['message' => 'Fee deleted']);
        } else {
            return abort(500, 'Fee not deleted');
        }
    }
}
