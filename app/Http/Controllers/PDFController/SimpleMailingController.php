<?php

namespace App\Http\Controllers\PDFController;

use App\Http\Controllers\Controller;
use App\Interfaces\FileRepositoryInterface;
use App\Models\Project;
use App\Repositories\FileRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

class SimpleMailingController extends Controller
{

    public function __construct(FileRepositoryInterface $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    public function generatePDF(Request $request, int $project_id, int $project_step_id)
    {
        /*     $projects = Project::find($project_id); */
        $project = Project::find($project_id);

        if ($request->has('simple')) {
            $view = 'pdf/simple_mailing';
            $output = "Courrier-simple_" . date("d-m-Y") . ".pdf";
        }
        if ($request->has('demeure')) {
            $view = 'pdf/demeure_mailing';
            $output = "Mise-demeure_" . date("d-m-Y") . ".pdf";
        }

        $data = [
            'creance' => $project->amount_ttc / 100,
            'id' => $project->id,
            'debtor' => [
                'name' => $project->debtor->last_name,
                'address' => $project->debtor->address . ',<br/> ' . $project->debtor->postcode . ' ' . $project->debtor->city,
            ],
            'billFiles' => $project->billFiles,
        ];



        $pdf = PDF::loadView($view, $data)->setOptions(['defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, "isPhpEnabled" => true]);
        $this->fileRepository->storePDFFile($pdf->output(), $output, $project->client_id, $project_id, $project_step_id);

        /*  return $pdf->stream(); */
        return $pdf->download($output);
    }
}
