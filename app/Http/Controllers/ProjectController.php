<?php

namespace App\Http\Controllers;

use App\Enum\ProjectStatusEnum;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Exports\ProjectsExport;
use Illuminate\Support\Facades\Cookie;
use App\Interfaces\ProjectRepositoryInterface;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{

    private ProjectRepositoryInterface $projectRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('project_type')) {
            if ($request->project_type == ProjectStatusEnum::OPEN->value)
                $projects = $this->projectRepository->getAllProjectsFeesBilled($request->client_id, $request->pattern, $request->fees_date['start'], $request->fees_date['end'], $request, $request->date_ignore);
            else if ($request->project_type == ProjectStatusEnum::CLOSE->value)
                $projects = $this->projectRepository->getAllProjectsBetween($request, $request->client_id, '1', $request->pattern, $request->fees_date['start'], $request->fees_date['end'], $request->date_ignore);
            else if ($request->project_type == ProjectStatusEnum::BILLING->value)
                $projects = $this->projectRepository->getAllProjectsBetween($request, $request->client_id, '2', $request->pattern, $request->fees_date['start'], $request->fees_date['end'], $request->date_ignore);
        } else {
            $projects = $this->projectRepository->getAllProjects(true, $request, $request->client_id, $request->is_closed, $request->pattern);
        }

        return response()->json($projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        return response()->json($this->projectRepository->storeProject($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Int $project_id)
    {

        Cookie::queue('project_id', $project_id, 6000);

        return response()->json($this->projectRepository->getProject($project_id, $request));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectRequest  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $project_id)
    {
        return response()->json($this->projectRepository->updateProject($project_id, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $project_id)
    {

        if ($this->projectRepository->destroyProject($project_id)) {
            return response()->json(['message' => 'Project deleted']);
            Session::flash('success', 'Project deleted successfully');
        } else {
            return abort(500, 'Project not deleted');
        }
    }
    /**
     * Export the specified resource from storage.
     *
     */
    public function export(Request $request)
    {
        return (new ProjectsExport($request->date));
    }
}
