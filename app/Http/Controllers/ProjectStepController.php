<?php

namespace App\Http\Controllers;

use App\Models\Step;
use App\Models\Project;
use App\Models\ProjectStep;
use Illuminate\Http\Request;
use App\Enum\ProjectStatusEnum;
use App\Interfaces\ProjectRepositoryInterface;
use App\Interfaces\ProjectStepRepositoryInterface;
use App\Repositories\ProjectRepository;

class ProjectStepController extends Controller
{
    const CLOTURE_STEP_ID = 19; // Cloture
    const ENDING_STEP_GROUP_ID = 15; // Le client à payé

    private ProjectStepRepositoryInterface $projectStepRepository;
    private ProjectRepositoryInterface $projectRepository;

    public function __construct(ProjectStepRepositoryInterface $projectStepRepository, ProjectRepositoryInterface $projectRepository)
    {
        $this->projectStepRepository = $projectStepRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Int $project_id
     * @param Int $step_id
     * @return void
     */
    public function store(Request $request, Int $project_id, Int $step_id)
    {

        $project_step = $this->projectStepRepository->storeProjectStep($project_id, $step_id, $request->date);


        if (Step::find($step_id)->step_group_id >= self::ENDING_STEP_GROUP_ID) {
            $this->projectRepository->updateProjectStatus($project_id, ProjectStatusEnum::CLOSE->value);
        }

        return $project_step ? response()->json($project_step) :  abort(500, 'Project step not created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectStep  $projectStep
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectStep $projectStep)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectStep  $projectStep
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectStep $projectStep)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProjectStepRequest  $request
     * @param  \App\Models\ProjectStep  $projectStep
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $project_id, Int $step_id)
    {
        $project_step = ProjectStep::where('project_id', $project_id)
            ->where('step_id', $step_id)
            ->firstOrFail();

        $project_step = $project_step->update($request->all());

        return response()->json($project_step);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectStep  $projectStep
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $project_id, Int $step_id)
    {
        $project_step = ProjectStep::where('project_id', $project_id)
            ->where('step_id', $step_id)
            ->firstOrFail();

        $delete = $project_step->delete();
        if ($delete) {
            return response()->json(['message' => 'Step deleted']);
        } else {
            return abort(500, 'Step not deleted');
        }
    }
}
