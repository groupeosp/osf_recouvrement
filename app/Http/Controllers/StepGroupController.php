<?php

namespace App\Http\Controllers;

use App\Models\StepGroup;
use App\Http\Requests\StoreStepGroupRequest;
use App\Http\Requests\UpdateStepGroupRequest;

class StepGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStepGroupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStepGroupRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StepGroup  $stepGroup
     * @return \Illuminate\Http\Response
     */
    public function show(StepGroup $stepGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StepGroup  $stepGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(StepGroup $stepGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStepGroupRequest  $request
     * @param  \App\Models\StepGroup  $stepGroup
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStepGroupRequest $request, StepGroup $stepGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StepGroup  $stepGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(StepGroup $stepGroup)
    {
        //
    }
}
