<?php

namespace App\Http\Middleware;

use App\Enum\UserRoleEnum;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnsureIsClientOfUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $auth = Auth::user();
        // || (isset($_GET['client_id'])  && $_GET['client_id'] != $auth->client_id && UserRoleEnum::USER === $auth->role)
        if (isset($_GET['client_id']) && $_GET['client_id'] != $auth->client_id && UserRoleEnum::USER === $auth->role) {
            return redirect('/');
        }
        return $next($request);
    }
}
