<?php

namespace App\Http\Middleware;

use Closure;
use App\Enum\UserRoleEnum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class IsAtOSP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->role != UserRoleEnum::USER->value) {


            $ip_granted = Config::get('ipwhitelist.ip');
            $ip = request()->ip();

            if (in_array($ip, $ip_granted)) {
                return $next($request);
            }
        } else {
            return redirect()->secure('/', 301);
        }
    }
}
