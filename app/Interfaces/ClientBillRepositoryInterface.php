<?php

namespace App\Interfaces;

interface ClientBillRepositoryInterface
{
    public function storeClientBill($dateStart, $dateEnd, $dateIgnore, $clientId, $output, $name, $cegid_id);
    public function generateClientBillPDF($download_it,$dateStart, $dateEnd, $clientId, $dateIgnore, $array_id, $client_bill_id, $cegid_id, $store_it = false);

    public function showClientBill($id);
    public function destroyClientBill($id);
}
