<?php

namespace App\Interfaces;

interface ClientRepositoryInterface
{
    public function showClient($ClientId, $details);
    public function updateClient($ClientId, $newDetails);

    public function getClientStatsBetween($ClientId, $startDate, $endDate, $dateIgnore);
}
