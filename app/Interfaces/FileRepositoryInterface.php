<?php

namespace App\Interfaces;

interface FileRepositoryInterface
{
    public function getAllFiles($details);
    public function getFileById($fileId);
    public function storeFile($file,$clientId,$projectId,$projectStepId);
    public function storePDFFile($output,$name,$clientId, $projectId, $projectStepId);
    public function updateFile($fileId, array $newDetails);
    public function destroyFile($fileId);
}
