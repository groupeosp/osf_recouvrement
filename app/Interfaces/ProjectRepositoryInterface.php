<?php

namespace App\Interfaces;

interface ProjectRepositoryInterface
{
    public function getAllProjectsCollection($details, $client_id, $status, $pattern);
    public function getAllProjects($paginate, $details, $client_id, $status, $pattern);
    public function getAllProjectsFeesBilled($client_id, $pattern, $dateStart, $dateEnd, $details, $dateIgnore);
    public function getAllProjectsBetween($details, $client_id, $status, $pattern, $dateStart, $dateEnd, $dateIgnore);

    public function getProject($id, $details);
    public function storeProject($details);
    public function updateProject($id, $details);
    public function destroyProject($id);

    public function getTotalToBill($client_id, $dateStart, $dateEnd, $dateIgnore);
    public function updateProjectStatus($project_id, $status);
}
