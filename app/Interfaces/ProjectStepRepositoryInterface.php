<?php

namespace App\Interfaces;

interface ProjectStepRepositoryInterface
{
    public function storeProjectStep($project_id, $step_id, $date);
}
