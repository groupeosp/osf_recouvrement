<?php

namespace App\Models;

use App\Enum\BillTypeEnum;
use App\Enum\BillStatusEnum;
use App\Enum\PaiementTypeEnum;
use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bill extends Model
{
    use HasFactory, CreatedUpdatedBy, SoftDeletes;

    protected $attributes = [
        'paiement_date' => null,
        'status' => '1',
        'billing_amount_ht' => 0,
    ];

    protected $casts = [
        'status' => BillTypeEnum::class,
        'type' => BillStatusEnum::class,
        'paiement_type' => PaiementTypeEnum::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'project_id',
        'billing_date',
        'billing_amount_ttc',
        'billing_amount_ht',
        'paiement_date'
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
