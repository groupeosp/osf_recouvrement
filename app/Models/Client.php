<?php

namespace App\Models;

use App\Enum\ProjectStatusEnum;
use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory, CreatedUpdatedBy, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        "email",
        'address',
        'phone_number',
        'postcode',
        'city',
        'country',
        'mandat_name',
        'mandat_hashname',
        'mandat_size',
        'mandat_extension',
    ];

    /* protected $appends = [
        'incomesPendingTTC',
        'incomesBilledTTC',
        'totalProjectsPending',
        'totalProjectsBilled',
        'totalProjectBills',
    ]; */

    /*public function getIncomesPendingTTCAttribute()
    {
        return $this->projects->where('status', ProjectStatusEnum::OPEN)->sum('totalEngagedFeesTTC') + $this->projects->where('status', ProjectStatusEnum::OPEN)->sum('totalProjectFeesHT') + $this->projects->where('status',  ProjectStatusEnum::OPEN)->sum('totalCommissionTTC');
    }
      public function getIncomesBilledTTCAttribute()
    {
        return $this->projects->where('status', ProjectStatusEnum::BILLING)->sum('totalEngagedFeesTTC') + $this->projects->where('status', ProjectStatusEnum::BILLING)->sum('totalProjectFeesHT') + $this->projects->where('status', ProjectStatusEnum::BILLING)->sum('totalCommissionTTC');
    }
    public function getTotalProjectsPendingAttribute()
    {
        return $this->projects->where('status', ProjectStatusEnum::OPEN)->count();
    }
    public function getTotalProjectsBilledAttribute()
    {
        return $this->projects->where('status', ProjectStatusEnum::BILLING)->count();
    }
    public function getTotalProjectBillsAttribute()
    {
        return $this->projects->sum('bill_number');
    } */

    /**
     * RELATIONSHIPS
     */

    public function projects(): HasMany
    {
        return $this->hasMany(Project::class);
    }
    public function contracts(): HasMany
    {
        return $this->hasMany(Contract::class);
    }
    public function clientBills(): HasMany
    {
        return $this->hasMany(ClientBill::class);
    }
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
