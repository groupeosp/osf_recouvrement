<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Debtor extends Model
{
    use HasFactory, CreatedUpdatedBy, SoftDeletes;

    /**
     * The default values of attributes
     * */
    protected $attributes = [
        'country' => 'France',
        'rib' => "",
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'last_name',
        'address',
        'postcode',
        'city',
        'country',
        'email',
        'phone_number',
        'rcs',
        'rib'
    ];

    public function projects(): HasMany
    {
        return $this->hasMany(Project::class);
    }
}
