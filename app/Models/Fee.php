<?php

namespace App\Models;

use App\Enum\FeeTypeEnum;
use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Fee extends Model
{
    use HasFactory, CreatedUpdatedBy, SoftDeletes;

    protected $casts = [
        'type' => FeeTypeEnum::class,
    ];

    protected $fillable = [
        'type',
        'project_id',
        'date',
        'name',
        'amount_ttc',
        'amount_ht',
    ];

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
