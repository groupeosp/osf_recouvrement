<?php

namespace App\Models;

use App\Enum\FileTypeEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class File extends Model
{
    use HasFactory, CreatedUpdatedBy, SoftDeletes;


    protected $casts = [
        'type' => FileTypeEnum::class,
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'step_file_id',
        'name',
        'last_name',
        'extension',
        'size',
        'bill_id',
        'bill_title',
        'bill_date',
        'bill_amount_ttc',
        'bill_amount_ht',
        'bill_left',
    ];

    public function projectStep(): BelongsTo
    {
        return $this->belongsTo(ProjectStep::class);
    }
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
