<?php

namespace App\Models;

use App\Models\Fee;
use App\Models\Step;
use App\Enum\BillStatusEnum;
use App\Enum\FeeTypeEnum;
use App\Enum\FileTypeEnum;
use App\Enum\ProjectStatusEnum;

use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use phpDocumentor\Reflection\Types\Null_;

class Project extends Model
{
    use HasFactory, CreatedUpdatedBy, SoftDeletes;

    /**
     * The default values for attributes
     */
    protected $attributes = [
        'amount_ttc' => 0,
        'amount_ht' => 0,
        'bill_number' => 0,
        'status' => ProjectStatusEnum::OPEN,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'client_id',
        'debtor_id',
        'amount_ttc',
        'amount_ht',
        'bill_number',
        'status'
    ];

    protected $appends = [
        'nextGroupStep',
        'currentStepName',
        'currentStep',
        "currentStepGroup",
        "currentPhase",
        "currentProjectSteps",
        "billSum",
        "lastBillingDate",
        "lastBillingAmount",
        "getFees",
        "totalEngagedFeesTTC",
        "totalEngagedFeesHT",
        "totalComplementaryFeesTTC",
        "totalComplementaryFeesHT",
        "totalProjectFeesHT",
        "totalProjectFeesTTC",
        "billFiles",
        "billFilesNumber",
        "engagedFees",
        "projectFees",
        "totalCommissionTTC",
        "totalCommissionHT"
    ];

    protected $casts = [
        'status' => ProjectStatusEnum::class,
    ];

    public function getTotalCommissionHTAttribute()
    {
        if ($this->billSum <= $this->amount_ttc) {
            return $this->billSum;
        } else {
            return $this->amount_ttc;
        }
    }
    public function getTotalCommissionTTCAttribute()
    {
        return $this->totalCommissionHT * 1.2;
    }
    public function getProjectFeesAttribute()
    {
        return $this->steps;
    }
    public function getEngagedFeesAttribute()
    {
        return $this->fees->where('type', FeeTypeEnum::ENGAGED_FEE);
    }
    public function getTotalProjectFeesHTAttribute()
    {
        return $this->steps->sum('default_price_ht');
    }
    public function getTotalProjectFeesTTCAttribute()
    {
        return $this->steps->sum('default_price_ht') * (1 + 20 / 100);
    }

    public function getTotalComplementaryFeesTTCAttribute()
    {
        return $this->fees->where('type', FeeTypeEnum::COMPLEMENTARY_FEE)->sum('amount_ttc');
    }
    public function getTotalComplementaryFeesHTAttribute()
    {
        return $this->fees->where('type', FeeTypeEnum::COMPLEMENTARY_FEE)->sum('amount_ht');
    }

    public function getTotalEngagedFeesTTCAttribute()
    {
        return $this->engagedFees->sum('amount_ttc');
    }
    public function getTotalEngagedFeesHTAttribute()
    {
        return $this->engagedFees->sum('amount_ht');
    }
    public function getGetFeesAttribute()
    {
        return $this->steps()->orderByPivot('date', 'DESC')->select('steps.name', 'default_price_ht')->where('default_price_ht', '>', 0)->get();
    }

    public function getLastBillingAmountAttribute()
    {
        $bill_amount_ttc = $this->bills() && count($this->bills) > 0 ? $this->bills()->latest()->first()->billing_amount_ttc : 0;
        return $bill_amount_ttc;
    }
    public function getLastBillingDateAttribute()
    {
        $bill_date = $this->bills() && count($this->bills) > 0 ? $this->bills()->latest()->first()->created_at : now();
        return $bill_date;
    }
    /**
     * Get sum of all bills
     *
     * @return void
     */
    public function getBillSumAttribute()
    {
        $bills_sum = $this->bills() ? $this->bills()->where('status', BillStatusEnum::VALIDATED->value)->sum('billing_amount_ttc') : 0;
        return intval($bills_sum);
    }

    /**
     * Return the next id group of step
     *
     * @return object|null
     */
    public function getNextGroupStepAttribute(): null|object
    {
        $stepGroups = StepGroup::find($this->currentStep?->step_group_id + 1);
        return $stepGroups ? $stepGroups : null;
    }
    /**
     * Return the name of the current step
     *
     * @return string
     */
    public function getCurrentStepNameAttribute(): string
    {
        $lastProjectStep = $this->lastProjectSteps->first();
        $lastStep =  $this->currentStep;
        return $lastProjectStep ? ($lastProjectStep->name ? $lastProjectStep->name : $lastStep->name)  : "Aucune étape";
    }
    /**
     * Return the current step
     *
     * @return object|null
     */
    public function getCurrentStepAttribute(): object|null
    {
        return  $this->lastProjectSteps?->first()?->step;
    }
    /**
     * Return the current step group
     *
     * @return object|null
     */
    public function getCurrentStepGroupAttribute(): object|null
    {
        $step_group_id =  $this->lastProjectSteps?->first()?->step->step_group_id;
        $stepGroups = StepGroup::find($step_group_id);
        return $stepGroups ? $stepGroups : null;
    }
    /**
     * Return the current phase name
     *
     * @return string
     */
    public function getCurrentPhaseAttribute(): string
    {
        return $this->currentStep ? $this->currentStep->phase->name : "Inconnu";
    }
    /**
     * Return the current project_steps
     *
     * @return object|null
     */
    public function getCurrentProjectStepsAttribute(): object|null
    {
        return  $this->lastProjectSteps?->first();
    }

    public function getBillFilesAttribute(): array|null
    {
        $bill_files = [];
        foreach ($this->projectSteps as $key => $value) {
            foreach ($value->files as $key2 => $el) {
                if ($el->type == FileTypeEnum::BILLS) {
                    $bill_files[] = $el;
                }
            }
        }
        return $bill_files ? $bill_files : null;
    }
    public function getBillFilesNumberAttribute(): int|null
    {
        return count($this->billFiles ?? []);
    }

    /**
     * RelationShip
     */

    public function debtor(): BelongsTo
    {
        return $this->belongsTo(Debtor::class);
    }
    public function bills(): HasMany
    {
        return $this->hasMany(Bill::class);
    }
    public function fees(): HasMany
    {
        return $this->hasMany(Fee::class);
    }
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
    public function steps(): BelongsToMany
    {
        return $this->belongsToMany(Step::class)->withPivot('name', 'created_at', 'date');
    }
    public function lastSteps(): BelongsToMany
    {
        return $this->belongsToMany(Step::class)->withPivot('name', 'created_at')->latest();
    }
    public function projectSteps(): HasMany
    {
        return $this->hasMany(ProjectStep::class);
    }
    public function lastProjectSteps(): HasMany
    {
        return $this->hasMany(ProjectStep::class)->latest();
    }
    public function clientBill(): BelongsTo
    {
        return $this->belongsTo(ClientBill::class);
    }
}
