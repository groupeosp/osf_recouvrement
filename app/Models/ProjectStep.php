<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProjectStep extends Model
{
    use HasFactory, CreatedUpdatedBy;

    protected $table = 'project_step';

    protected $attributes = [];

    protected $fillable = [
        'name',
        'project_id',
        'step_id',
        'date'
    ];

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }
    public function files(): HasMany
    {
        return $this->hasMany(File::class);
    }
    public function notes(): HasMany
    {
        return $this->hasMany(Note::class);
    }
    public function step(): BelongsTo
    {
        return $this->belongsTo(Step::class);
    }
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
