<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\CreatedUpdatedBy;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Step extends Model
{
    use HasFactory, CreatedUpdatedBy;

    /**
     * The default values for attributes
     */
    protected $attributes = [
        'default_price_ht' => 0,
        'is_visible' => 1,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'phase_id',
        'name',
        'is_visible',
        'sort_number',
        'default_price_ht'
    ];


    public function project(): BelongsToMany
    {
        return $this->belongsToMany(Project::class);
    }
    public function phase(): BelongsTo
    {
        return $this->belongsTo(Phase::class);
    }
    public function stepGroup(): BelongsTo
    {
        return $this->belongsTo(StepGroup::class);
    }
}
