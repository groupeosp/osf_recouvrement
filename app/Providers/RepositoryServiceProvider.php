<?php

namespace App\Providers;

use App\Repositories\FileRepository;
use App\Repositories\ClientRepository;
use App\Repositories\ProjectRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\ClientBillRepository;
use App\Interfaces\FileRepositoryInterface;
use App\Repositories\ProjectStepRepository;
use App\Interfaces\ClientRepositoryInterface;
use App\Interfaces\ProjectRepositoryInterface;
use App\Interfaces\ClientBillRepositoryInterface;
use App\Interfaces\ProjectStepRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FileRepositoryInterface::class, FileRepository::class);
        $this->app->bind(ProjectRepositoryInterface::class, ProjectRepository::class);
        $this->app->bind(ClientRepositoryInterface::class, ClientRepository::class);
        $this->app->bind(ClientBillRepositoryInterface::class, ClientBillRepository::class);
        $this->app->bind(ProjectStepRepositoryInterface::class, ProjectStepRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
