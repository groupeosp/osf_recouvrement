<?php

namespace App\Repositories;

use App\Enum\ProjectStatusEnum;
use App\Models\Client;
use PDF;
use App\Models\ClientBill;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\ProjectRepositoryInterface;
use App\Interfaces\ClientBillRepositoryInterface;
use App\Interfaces\FileRepositoryInterface;

class ClientBillRepository implements ClientBillRepositoryInterface
{
    const FACTURE_STEP_ID = 20;

    private ProjectRepositoryInterface $projectRepository;
    private FileRepositoryInterface $fileRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository, FileRepositoryInterface $fileRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->fileRepository = $fileRepository;
    }

    public function storeClientBill($dateStart, $dateEnd, $dateIgnore, $clientId, $output, $name, $cegid_id)
    {
        $total = $this->projectRepository->getTotalToBill($clientId, $dateStart, $dateEnd, $dateIgnore);

        $hash_name = str_replace('/', '', Hash::make($name));
        $path = 'clients/' . $clientId . '/bills/';

        $path = Storage::put(
            $path . $hash_name . '.pdf',
            $output
        );


        if ($path) {

            $clientBill = new ClientBill();
            $clientBill->cegid_id = $cegid_id;
            $clientBill->client_id = $clientId;
            $clientBill->amount_ht = $total;
            $clientBill->amount_ttc = $total;
            $clientBill->name = explode(".", $name)[0];
            $clientBill->hashname = $hash_name;
            $clientBill->date = now();
            $clientBill->save();

            return $clientBill->id;
        }
        return abort(500, 'Storage has failed');
    }

    public function generateClientBillPDF($download_it, $dateStart, $dateEnd, $clientId, $dateIgnore, $array_id, $client_bill_id, $cegid_id, $store_it = false)
    {

        $name = "Recap_facture_" . $dateStart . "_" . $dateEnd . ".pdf";
        $client = Client::findOrFail($clientId);

        $projects = $this->projectRepository->getAllProjectsBetween(null, $clientId, ProjectStatusEnum::CLOSE->value,  '', $dateStart, $dateEnd, $dateIgnore)
            ->whereIn('id', $array_id);

        $data = [
            'client' => $client,
            'projects' => $projects,
            'cegid_id' => $cegid_id,
            'bill_id' => ClientBill::count(),
        ];


        $pdf = PDF::loadView('pdf/bill_mailing', $data)->setOptions(['defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, "isPhpEnabled" => true]);

        if ($store_it) {
            $bill_id =  $this->storeClientBill($dateStart, $dateEnd, $dateIgnore, $clientId, $pdf->output(), $name, $cegid_id);
            //Link bill to projects
            foreach ($projects as $project) {
                $project->client_bills_id = $bill_id;
                $project->save();
            }
        }


        if ($download_it) {
            return $pdf;
        } else
            return compact('bill_id', 'pdf');
    }
    public function showClientBill($id)
    {
        return ClientBill::select(['name', 'hashname', 'client_id', 'id'])->findOrFail($id);
    }
    public function destroyClientBill($id)
    {
        $clientBill = ClientBill::findOrFail($id);
        return $clientBill->delete();
    }
}
