<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Client;
use Carbon\CarbonPeriod;
use App\Enum\ProjectStatusEnum;
use App\Interfaces\ClientRepositoryInterface;
use App\Interfaces\ProjectRepositoryInterface;

class ClientRepository implements ClientRepositoryInterface
{

    private ProjectRepositoryInterface $projectRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    public function showClient($ClientId, $details)
    {
        $client = Client::when($details->has('contracts'), function ($query) {
            return $query->with('contracts');
        })
            ->when($details->has('client_bill'), function ($query) use ($details) {

                return $query->when($details->has('date_start') && $details->has('date_end'), function ($query) use ($details) {
                    return $query->with(['clientBills' => function ($query) use ($details) {
                        return $query->whereBetween('date', [$details->get('date_start'), $details->get('date_end')])->with('createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name');
                    }]);
                });
                /*  return $query->with('clientBills.createdBy:id,first_name,last_name', 'clientBills.updatedBy:id,first_name,last_name'); */
            })
            ->with([
                'createdBy:id,first_name,last_name',
                'updatedBy:id,first_name,last_name',
            ])
            ->findOrFail($ClientId);

        if ($details->has('date_start') && $details->has('date_end')) {
            $stats = $this->getClientStatsBetween($ClientId, $details->date_start, $details->date_end, $details->has('date_ignore'));

            $client->incomesPendingTTC = $stats['incomesPendingTTC'];
            $client->incomesComplementaryTTC = $stats['incomesComplementaryTTC'];
            $client->totalProjectsPending = $stats['totalProjectsPending'];
            $client->totalProjectsBilled = $stats['totalProjectsBilled'];
            $client->totalProjectBills = $stats['totalProjectBills'];
            $client->dateInterval = $stats['dateInterval'];
            /*             $client->totalPendingInterval = $stats['totalPendingInterval']; */
            $client->totalBilledPendingInterval = $stats['totalBilledPendingInterval'];
            $client->totalComplementatyFeesInterval = $stats['totalComplementatyFeesInterval'];
            $client->totalEngagedFeesInterval = $stats['totalEngagedFeesInterval'];
            $client->totalEngagedFeesPendingInterval = $stats['totalEngagedFeesPendingInterval'];
        }

        return $client;
    }
    public function updateClient($ClientId, $newDetails)
    {
        return  Client::findOrFail($ClientId)->update($newDetails);
    }

    public function getClientStatsBetween($ClientId, $startDate, $endDate, $dateIgnore)
    {

        $client = Client::findOrFail($ClientId);


        $pendingProjects = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::OPEN->value, '', $startDate, $endDate, $dateIgnore);
        $billingProjects = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::BILLING->value, '', $startDate, $endDate, $dateIgnore);

        /*   $incomesPendingTTC = $pendingProjects->sum('totalEngagedFeesTTC') + $pendingProjects->sum('totalProjectFeesHT') + $pendingProjects->sum('totalCommissionTTC');
        $incomesBilledTTC = $billingProjects->sum('totalEngagedFeesTTC') + $billingProjects->sum('totalProjectFeesHT') + $billingProjects->sum('totalCommissionTTC'); */

        $totalProjectsPending = $pendingProjects->count();
        $totalProjectsBilled = $billingProjects->count();

        $totalProjectBills =  $client->projects->sum('bill_number');

        $dateInterval = CarbonPeriod::create($startDate, $endDate);

        $totalEngagedFeesInterval = [];
        $totalComplementatyFeesInterval = [];

        $totalEngagedFeesPendingInterval = [];
        $totalBilledPendingInterval = [];

        foreach ($dateInterval as $date) {
            $pendingProjects_tmp = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::OPEN->value, '', $date, $date, false);
            $billingProjects_tmp  = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::BILLING->value, '', $date, $date, false);

            $pendingProjects_tmp_all = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::OPEN->value, '', $date, $date, true);
            $closedProjects_tmp_all = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::CLOSE->value, '', $date, $date, true);
            $billedProjects_tmp_all = $this->projectRepository->getAllProjectsBetween(null, $ClientId, ProjectStatusEnum::BILLING->value, '', $date, $date, true);

            /**
             * Requete sur les 
             * - frais engagés de la période peut importe le status
             * - frais complementaires de la période peut importe le status
             * 
             * - frais engagés des dossiers en cours de paiement de la période FAIT
             * - total facturé sur la periode FAIT
             */
            $totalEngagedFeesInterval[] = $pendingProjects_tmp_all->sum('totalEngagedFeesTTC') + $closedProjects_tmp_all->sum('totalEngagedFeesTTC') + $billedProjects_tmp_all->sum('totalEngagedFeesTTC');
            $totalComplementatyFeesInterval[] = $pendingProjects_tmp_all->sum('totalProjectFeesTTC') + $closedProjects_tmp_all->sum('totalProjectFeesTTC') + $billedProjects_tmp_all->sum('totalProjectFeesTTC');

            $totalEngagedFeesPendingInterval[] = $pendingProjects_tmp->sum('totalEngagedFeesTTC');
            $totalBilledPendingInterval[] = $billingProjects_tmp->sum('totalEngagedFeesTTC') + $billingProjects_tmp->sum('totalProjectFeesTTC') + $billingProjects_tmp->sum('totalCommissionTTC');
        }

        $incomesPendingTTC = $totalEngagedFeesInterval[0];
        $incomesComplementaryTTC = $totalComplementatyFeesInterval[0];


        return compact('totalComplementatyFeesInterval', 'totalEngagedFeesInterval', 'incomesPendingTTC', 'incomesComplementaryTTC', 'totalProjectsPending', 'totalProjectsBilled', 'totalProjectBills', 'dateInterval', 'totalBilledPendingInterval', 'totalEngagedFeesPendingInterval');
    }
}
