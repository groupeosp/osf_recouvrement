<?php

namespace App\Repositories;

use App\Models\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\FileRepositoryInterface;

class FileRepository implements FileRepositoryInterface
{
    public function getAllFiles($details)
    {
        $files = File::when($details->has('project_step_id'), function ($query) use ($details) {
            return $query->where('project_step_id', $details->get('project_step_id'));
        })->with(['createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name'])->get();

        return response()->json($files);
    }

    public function getFileById($fileId)
    {
        return File::select(['project_step_id', 'name', 'hashname', 'extension'])->with('projectStep.project')->findOrFail($fileId);;
    }


    public function storeFile($file, $clientId, $projectId, $projectStepId)
    {
        if (isset($file) && is_file($file)) {
            // Prevent from '/' in the filename
            $hash_name = str_replace('/', '', Hash::make($file->getClientOriginalName()));
            $path = 'clients/' . $clientId . '/projects/' . $projectId . '/files';

            $path = Storage::putFileAs(
                $path,
                $file,
                $hash_name . '.' . $file->getClientOriginalExtension()
            );

            if ($path) {

                $newFile = new File();
                $newFile->project_step_id = $projectStepId;
                $newFile->name = explode(".", $file->getClientOriginalName())[0];
                $newFile->hashname = $hash_name;
                $newFile->size = $file->getSize();
                $newFile->extension = $file->getClientOriginalExtension();
                $newFile->save();

                return response()->json($newFile);
            }
            return abort(500, 'Storage has failed');
        }
        return abort(500, 'File not found');
    }
    public function storePDFFile($output, $name, $clientId, $projectId, $projectStepId)
    {
        $hash_name = str_replace('/', '', Hash::make($name));
        $path = 'clients/' . $clientId . '/projects/' . $projectId . '/files/';

        $path = Storage::put(
            $path . $hash_name . '.pdf',
            $output
        );

        if ($path) {

            $newFile = new File();
            $newFile->project_step_id = $projectStepId;
            $newFile->name = explode(".", $name)[0];
            $newFile->hashname = $hash_name;
            $newFile->size = 0;
            $newFile->extension = "pdf";
            $newFile->save();

            return response()->json($newFile);
        }
        return abort(500, 'Storage has failed');
    }

    public function updateFile($fileId, array $newDetails)
    {
        $file = File::where('id', $fileId)->firstOrFail()->update($newDetails);
        return response()->json($file);
    }

    public function destroyFile($fileId)
    {
        $file = File::findOrFail($fileId)->delete();
        if ($file) {
            return response()->json(['message' => 'File deleted']);
        } else {
            return abort(500, 'File not deleted');
        }
    }
}
