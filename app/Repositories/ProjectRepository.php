<?php

namespace App\Repositories;

use App\Models\File;
use App\Models\Debtor;
use App\Models\Project;
use App\Enum\FileTypeEnum;
use App\Models\ProjectStep;
use App\Enum\ProjectStatusEnum;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Interfaces\ProjectRepositoryInterface;

class ProjectRepository implements ProjectRepositoryInterface
{

    const CLOTURE_STEP_ID = 19,
        FACTURE_STEP_ID = 20;


    public function getAllProjectsCollection($details, $client_id, $status, $pattern)
    {
        return Project::when(isset($pattern), function ($query) use ($pattern) {
            $query->where('id', 'LIKE', '%' . $pattern . '%');
        })
            ->when(isset($client_id), function ($query) use ($client_id) {
                $query->where('client_id', $client_id);
            })
            ->when(isset($status), function ($query) use ($status) {
                if ($status == '0') {
                    $query->where('status', ProjectStatusEnum::OPEN);
                } else if ($status == '1') {
                    $query->where('status', ProjectStatusEnum::CLOSE);
                } else {
                    $query->where('status', ProjectStatusEnum::BILLING);
                }
            })
            ->when(isset($details) && isset($details->order), function ($query) use ($details) {
                $query->orderBy($details->order['by'], $details->order['type']);
            })
            ->when(isset($details) && $details->has('time'), function ($query) {
                return $query->with(['createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name']);
            })
            ->when(isset($details) && $details->has('debtor'), function ($query) {
                return $query->with('debtor:id,last_name');
            })
            ->when(isset($details) && $details->has('bill'), function ($query) {
                return $query->with('bills');
            })
            ->when(isset($details) && $details->has('phase'), function ($query) {
                return $query->with('projectSteps.step.phase:id,name')->with('projectSteps.createdBy:id,first_name,last_name')->with('lastProjectSteps.step:id,name,step_group_id,is_visible,days_until_deadline,phase_id');
            });
    }
    public function getAllProjects($paginate = true, $details, $client_id, $status, $pattern)
    {
        $projects = $this->getAllProjectsCollection($details, $client_id, $status, $pattern);
        if ($paginate)
            return $projects->paginate(9);
        else
            return $projects->get();
    }
    public function getAllProjectsFeesBilled($client_id, $pattern, $dateStart, $dateEnd, $details, $dateIgnore)
    {
        $dateCarbon = [
            'start' => \Carbon\Carbon::parse($dateStart)->format('Y-m-d'),
            'end' => \Carbon\Carbon::parse($dateEnd)->format('Y-m-d'),

        ];

        return $this->getAllProjectsCollection($details, $client_id, "0", $pattern)->with([
            'steps' => function ($query) use ($dateCarbon, $dateIgnore) {
                $query->where('default_price_ht', '>', 0)->when(!$dateIgnore, function ($query)  use ($dateCarbon) {
                    $query->whereBetween('project_step.date', [$dateCarbon['start'], $dateCarbon['end']]);
                });
            },
            'fees' => function ($query) use ($dateCarbon, $dateIgnore) {
                $query->when($dateCarbon['start'] == $dateCarbon['end'] && !$dateIgnore, function ($query) use ($dateCarbon) {
                    $query->where('date', $dateCarbon['start']);
                })->when($dateCarbon['start'] != $dateCarbon['end'] && !$dateIgnore, function ($query) use ($dateCarbon) {
                    $query->whereBetween('date', [$dateCarbon['start'], $dateCarbon['end']]);
                });
            }
        ])->where(function ($query) use ($dateCarbon, $dateIgnore) {
            $query->whereHas('steps', function ($query) use ($dateCarbon, $dateIgnore) {
                $query->where('default_price_ht', '>', 0)
                    ->when(!$dateIgnore, function ($query)  use ($dateCarbon) {
                        $query->where('project_step.date', '<=', $dateCarbon['end'])->where('project_step.date', '>=', $dateCarbon['start']);
                    });
            })->orWhereHas('fees', function ($query) use ($dateCarbon, $dateIgnore) {
                $query->when($dateCarbon['start'] == $dateCarbon['end'] && !$dateIgnore, function ($query) use ($dateCarbon) {
                    $query->where('date', date($dateCarbon['start']));
                })->when($dateCarbon['start'] != $dateCarbon['end'] && !$dateIgnore, function ($query) use ($dateCarbon) {
                    $query->whereBetween('date', [$dateCarbon['start'], $dateCarbon['end']]);
                });
            });
        })->get();
    }
    public function getProject($id, $details)
    {
        $project = Project::when($details->has('debtor'), function ($query) {
            return $query->with('debtor:id,last_name');
        })
            ->when($details->has('time'), function ($query) {
                return $query->with(['createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name']);
            })
            ->when($details->has('project_step'), function ($query) use ($details) {

                return $query->with(['lastProjectSteps.step:id,name,step_group_id,is_visible,days_until_deadline,phase_id', 'lastProjectSteps.createdBy:id,first_name,last_name'])

                    ->when($details->has('file'), function ($query) {
                        return $query->with(['lastProjectSteps.files.createdBy:id,first_name,last_name', 'lastProjectSteps.files.updatedBy:id,first_name,last_name']);
                    });
            })
            ->when($details->has('bill'), function ($query) {
                return $query->with('bills');
            })
            ->when($details->has('timestamps'), function ($query) {
                return $query->with('createdBy:id,first_name,last_name', 'updatedBy:id,first_name,last_name');
            })
            ->findOrFail($id);

        return $project;
    }
    public function storeProject($details)
    {
        //Get the files in an array
        $files = [];
        $matches = [];

        // We put on an array the files that are uploaded 
        // we must check also the type and push it into the array
        foreach ($details as $key => $value) {
            if (str_contains($key, 'files-')) {
                preg_match('/(\d+)$/', $key, $matches);
                array_push($files, ['file' => $value, 'complement' =>
                [
                    'name' => $details['file-complement-name-' . $matches[0]],
                    'type' => $details['file-complement-type-' . $matches[0]],
                    'bill_id' => $details['file-complement-bill_id-' . $matches[0]],
                    'bill_date' => $details['file-complement-bill_date-' . $matches[0]],
                    'bill_title' => $details['file-complement-bill_title-' . $matches[0]],
                    'bill_amount_ttc' => $details['file-complement-bill_amount_ttc-' . $matches[0]],
                    'bill_left' => $details['file-complement-bill_left-' . $matches[0]],
                ]]);
            }
        }


        //If client already exists, we update it else we create it
        $debtor = Debtor::updateOrCreate(
            ['id' => $details['id'] ? $details['id'] : 0],
            [
                'last_name' => $details['last_name'],
                'email' => $details['email'],
                'phone_number' => $details['phone_number'],
                'address' => $details['address'],
                'postcode' => $details['postcode'],
                'city' => $details['city'],
                'rib' => $details['rib'],
                'rcs' => $details['rcs'],
            ]
        );


        $project = new Project();
        $project->client_id = $details['client_id'];
        $project->debtor_id = $debtor->id;
        $project->status = 0;
        $project->amount_ttc = $details['amount_ttc'] * 100;
        $project->bill_number = $details['bill_number'];
        $project->save();

        $project_step = new ProjectStep();
        $project_step->date = now();
        $project_step->project_id = $project->id;
        $project_step->step_id = 1;
        $project_step->save();


        foreach ($files as $key => $value) {
            $hash_name = str_replace('/', '', Hash::make($value['file']->getClientOriginalName()));
            $path = 'clients/' . $details['client_id'] . '/projects/' . $project->id . '/files';

            $path = Storage::putFileAs(
                $path,
                $value['file'],
                $hash_name . '.' . $value['file']->getClientOriginalExtension()
            );

            if ($path) {

                $file = new File();

                switch ($value['complement']['type']) {
                    case FileTypeEnum::FILES->value:
                        $file->type = FileTypeEnum::FILES->value;
                        break;
                    case FileTypeEnum::BILLS->value:
                        $file->type = FileTypeEnum::BILLS->value;
                        $file->bill_id = $value['complement']['bill_id'];
                        $file->bill_date = $value['complement']['bill_date'];
                        $file->bill_title = $value['complement']['bill_title'];
                        $file->bill_amount_ttc = $value['complement']['bill_amount_ttc'] * 100;
                        $file->bill_left = $value['complement']['bill_left'] * 100;
                        break;
                    default:
                        $file->type = FileTypeEnum::FILES->value;
                        break;
                }

                $file->project_step_id = $project_step->id;
                $file->name = $value['complement']['name'] != "" ?  $value['complement']['name'] : explode(".", $value['file']->getClientOriginalName())[0];
                $file->hashname = $hash_name;
                $file->size = $value['file']->getSize();
                $file->extension = $value['file']->getClientOriginalExtension();
                $file->save();
            }
        }

        return $project->id;
    }
    public function updateProject($id, $details)
    {
        $project = Project::where('id', $id)->firstOrFail();
        $project = $project->update($details);
    }
    public function destroyProject($id)
    {
        $project = Project::findOrFail($id)->delete();

        return $project;
    }

    public function getTotalToBill($client_id, $dateStart, $dateEnd, $dateIgnore)
    {
        $projects = $this->getAllProjectsFeesBilled($client_id, '', $dateStart, $dateEnd, null, $dateIgnore)->all();
        $sum = array_reduce($projects, function ($carry, $item) {
            return $carry += ($item['totalEngagedFeesTTC'] + $item['totalProjectFeesHT']);
        }, 0);

        return $sum;
    }

    public function getAllProjectsBetween($details, $client_id, $status, $pattern, $dateStart, $dateEnd, $dateIgnore)
    {
        $status_steps = [
            ProjectStatusEnum::CLOSE->value => self::CLOTURE_STEP_ID,
            ProjectStatusEnum::BILLING->value => self::FACTURE_STEP_ID,
        ];

        if ($status == ProjectStatusEnum::OPEN->value) {
            return $this->getAllProjectsFeesBilled($client_id, $pattern, $dateStart, $dateEnd, $details, $dateIgnore);
        }

        return $this->getAllProjectsCollection($details, $client_id, $status, $pattern)
            ->when(!$dateIgnore, function ($query)  use ($dateStart, $dateEnd, $status_steps, $status) {
                $query->whereHas('projectSteps', function ($query) use ($dateStart, $dateEnd, $status_steps, $status) {
                    $query->where('step_id', $status_steps[$status])->whereBetween('date', [$dateStart, $dateEnd]);
                });
            })->get();
    }

    public function updateProjectStatus($project_id, $status)
    {
        $project = Project::findOrFail($project_id);
        $project->status = $status;
        $project->save();
    }
}
