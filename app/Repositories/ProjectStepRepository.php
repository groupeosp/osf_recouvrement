<?php

namespace App\Repositories;

use App\Models\ProjectStep;
use App\Interfaces\ProjectStepRepositoryInterface;

class ProjectStepRepository implements ProjectStepRepositoryInterface
{
    public function storeProjectStep($project_id, $step_id, $date)
    {
        return ProjectStep::create([
            'project_id' => $project_id,
            'step_id' => $step_id,
            'date' => $date
        ]);
    }
}
