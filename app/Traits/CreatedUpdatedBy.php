<?php

namespace App\Traits;

use Illuminate\Support\Facades\Config;

trait CreatedUpdatedBy
{

    public static function bootCreatedUpdatedBy()
    {

        // updating created_by and updated_by when model is created
        static::creating(function ($model) {
            if (!$model->isDirty('created_by')) {
                /*  $model->created_at = now(); */
                $model->created_by = auth()->user() ? auth()->user()->id : Config::get('constants.ADMIN_DEFAULT_ID');
            }
            if (!$model->isDirty('updated_by')) {
                /* $model->updated_at = now(); */
                $model->updated_by = auth()->user() ? auth()->user()->id : Config::get('constants.ADMIN_DEFAULT_ID');
            }
        });

        // updating updated_by when model is updated
        static::updating(function ($model) {
            if (!$model->isDirty('updated_by')) {
                /*  $model->updated_at = now(); */
                $model->updated_by = auth()->user() ? auth()->user()->id : Config::get('constants.ADMIN_DEFAULT_ID');
            }
        });

        // updating desactived_by when model is deleted
        static::deleting(function ($model) {
            /*  if (!$model->isDirty('desactived_by')) {
                $model->desactived_by = auth()->user() ? auth()->id : Config::get('constants.ADMIN_DEFAULT_ID');
            } */
            if (!$model->isDirty('updated_by')) {
                $model->updated_by = auth()->user() ? auth()->user()->id : Config::get('constants.ADMIN_DEFAULT_ID');
            }
        });
    }
}
