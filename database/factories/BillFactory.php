<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Bill>
 */
class BillFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'billing_date' => now(),
            'billing_amount_ht' => $this->faker->numberBetween(10, 10000),
            'billing_amount_ttc' => $this->faker->numberBetween(10, 10000),
            'paiement_date' => now()
        ];
    }
}
