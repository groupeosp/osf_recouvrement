<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Debtor>
 */
class DebtorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'last_name' => $this->faker->name,
            'address' => $this->faker->address,
            'postcode' => $this->faker->postcode,
            'city' => $this->faker->city,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
        ];
    }
}
