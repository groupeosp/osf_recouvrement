<?php

namespace Database\Factories;

use App\Enum\FeeTypeEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Fee>
 */
class FeeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'type' => FeeTypeEnum::ENGAGED_FEE,
            'name' => $this->faker->name,
            'amount_ttc' => $this->faker->numberBetween(10, 10000),
            'amount_ht' => $this->faker->numberBetween(10, 10000),
            'date' => now()->format('Y-m-d'),
        ];
    }
}
