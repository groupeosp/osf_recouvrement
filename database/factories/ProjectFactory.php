<?php

namespace Database\Factories;

use App\Enum\ProjectStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Project>
 */
class ProjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'status' => ProjectStatusEnum::CLOSE,
            'client_id' => 2,
            'debtor_id' => $this->faker->numberBetween(1, 5),
            'amount_ttc' => $this->faker->numberBetween(10, 10000),
            'amount_ht' => $this->faker->numberBetween(10, 10000),
            'bill_number' => $this->faker->numberBetween(1, 5),
        ];
    }
}
