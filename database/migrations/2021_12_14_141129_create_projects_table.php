<?php

use App\Enum\ProjectStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('debtor_id');
            $table->unsignedBigInteger('client_bills_id')->nullable();
            $table->unsignedInteger('amount_ttc');
            $table->integer('amount_ht');
            $table->unsignedInteger('bill_number');
            // Enum role type
            $table->enum('status', [
                ProjectStatusEnum::CLOSE->value,
                ProjectStatusEnum::OPEN->value,
                ProjectStatusEnum::BILLING->value,
            ])->default(ProjectStatusEnum::OPEN->value);

            $table->softDeletes();

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->timestamps();

            /**
             * FOREIGN KEYS
             */
            $table->foreign('debtor_id')->references('id')->on('debtors')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('client_bills_id')->references('id')->on('client_bills')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
};
