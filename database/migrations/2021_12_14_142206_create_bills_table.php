<?php

use App\Enum\BillTypeEnum;
use App\Enum\Paiement_type;
use App\Enum\BillStatusEnum;
use App\Enum\PaiementTypeEnum;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->date('billing_date');
            $table->unsignedInteger('billing_amount_ttc');
            $table->unsignedInteger('billing_amount_ht');
            $table->timestamp('paiement_date')->nullable();
            $table->enum('status', [
                BillStatusEnum::PENDING->value,
                BillStatusEnum::VALIDATED->value,
            ])->default(BillStatusEnum::PENDING->value); // 0 -> pending | 1 -> validated
            $table->enum('type', [
                BillTypeEnum::CLIENT_PAYMENT->value,
                BillTypeEnum::OSP_PAYMENT->value,
            ])->default(BillTypeEnum::CLIENT_PAYMENT->value);
            $table->enum('paiement_type', [
                PaiementTypeEnum::CHEQUE->value,
                PaiementTypeEnum::CB->value,
                PaiementTypeEnum::VIREMENT->value,
            ])->default(PaiementTypeEnum::VIREMENT->value);

            $table->softDeletes();

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->timestamps();

            /**
             * FOREIGN KEY
             */
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
};
