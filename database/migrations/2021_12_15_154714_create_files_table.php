<?php

use App\Enum\FileTypeEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', [
                FileTypeEnum::FILES->value,
                FileTypeEnum::BILLS->value,
            ])->default(FileTypeEnum::FILES->value);
            $table->unsignedBigInteger('project_step_id');
            $table->string('name');
            $table->string('hashname');
            $table->string('extension');
            $table->unsignedInteger('size');

            $table->date('bill_date')->nullable();
            $table->string('bill_id')->nullable();
            $table->string('bill_title')->nullable();
            $table->unsignedInteger('bill_amount_ttc')->nullable();
            $table->unsignedInteger('bill_amount_ht')->nullable();
            $table->unsignedInteger('bill_left')->nullable();

            $table->softDeletes();

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->timestamps();

            /**
             * FOREIGN KEYS
             */
            $table->foreign('project_step_id')->references('id')->on('project_step')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
};
