<?php

use App\Enum\FeeTypeEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->enum('type', [
                FeeTypeEnum::ENGAGED_FEE->value,
                FeeTypeEnum::COMPLEMENTARY_FEE->value,
            ])->default(FeeTypeEnum::ENGAGED_FEE->value);
            $table->string('name');
            $table->unsignedInteger('amount_ttc');
            $table->unsignedInteger('amount_ht');
            $table->date('date');

            $table->softDeletes();

            $table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by');
            $table->timestamps();

            /**
             * FOREIGN KEY
             */
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees');
    }
};
