<?php

namespace Database\Seeders;

use App\Models\Fee;
use App\Models\Bill;
use App\Models\Step;
use App\Models\Phase;
use App\Models\Client;
use App\Models\Debtor;
use App\Models\Project;
use App\Models\ProjectStep;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * INITIATES VALUES
         */

        $this->call(ClientSeeder::class);
        /*   $this->call(DebtorSeeder::class);
        $this->call(PhaseSeeder::class);
        $this->call(StepSeeder::class); */


        $this->call(PhaseSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(StepGroupSeeder::class);
        $this->call(StepSeeder::class);

        Debtor::factory()->count(5)->has(
            Project::factory()->count(5)->has(
                Bill::factory()->count(1)
            )->has(
                Fee::factory()->count(5)
            )
        )->create();

        ProjectStep::factory()->count(20)->create();
    }
}
