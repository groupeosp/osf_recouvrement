<?php

namespace Database\Seeders;

use App\Models\Debtor;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DebtorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Debtor::factory()->count(10)->create();
    }
}
