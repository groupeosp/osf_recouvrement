<?php

namespace Database\Seeders;


use App\Models\Phase;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PhaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Phase::create([
            'id' => 1,
            'name' => 'Transmission',
        ]);
        Phase::create([
            'id' => 2,
            'name' => 'Amiable',
        ]);
        Phase::create([
            'id' => 3,
            'name' => 'Précontentieuse',
        ]);
        Phase::create([
            'id' => 4,
            'name' => 'Judiciaire',
        ]);
        Phase::create([
            'id' => 5,
            'name' => 'Clos',
        ]);
        Phase::create([
            'id' => 6,
            'name' => 'Facturé',
        ]);
    }
}
