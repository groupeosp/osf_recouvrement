<?php

namespace Database\Seeders;

use App\Models\StepGroup;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StepGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StepGroup::create([
            'id' => 1,
            'name' => 'Réception du dossier',
        ]);
        StepGroup::create([
            'id' => 2,
            'name' => 'Vérification du dossier',
            'is_multiple' => true
        ]);
        StepGroup::create([
            'id' => 3,
            'name' => 'Vérification de la société'
        ]);
        StepGroup::create([
            'id' => 4,
            'name' => 'Envoi du courrier',
            'is_multiple' => true
        ]);
        StepGroup::create([
            'id' => 5,
            'name' => 'Appels téléphoniques',
            'is_multiple' => true
        ]);
        StepGroup::create([
            'id' => 6,
            'name' => 'Mise en demeure'
        ]);
        StepGroup::create([
            'id' => 7,
            'name' => 'Requête IP'
        ]);
        StepGroup::create([
            'id' => 8,
            'name' => 'Retour du greffe du dossier IP',
            'is_multiple' => true
        ]);
        StepGroup::create([
            'id' => 9,
            'name' => 'Signification au débiteur'
        ]);
        StepGroup::create([
            'id' => 10,
            'name' => 'Retour du huissier avec l’acte signifié'
        ]);
        StepGroup::create([
            'id' => 11,
            'name' => 'Le client fait opposition ou non'
        ]);

        StepGroup::create([
            'id' => 12,
            'name' => 'Envoi du titre au huissier'
        ]);
        StepGroup::create([
            'id' => 13,
            'name' => 'Autre',
            'is_multiple' => true
        ]);
        StepGroup::create([
            'id' => 14,
            'name' => 'Le client a payé'
        ]);
        StepGroup::create([
            'id' => 15,
            'name' => 'Facturation'
        ]);
    }
}
