<?php

namespace Database\Seeders;

use App\Enum\StepGroupEnum;
use App\Models\Step;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Step::create([
            'phase_id' => 1,
            'name' => 'Réception du dossier',
            'is_visible' => 1,
            'sort_number' => 1,
            'step_group_id' => 1,
            'default_price_ht' => 0
        ]);
        Step::create([
            'phase_id' => 1,
            'name' => 'Demande pièces complémentaires',
            'is_visible' => 0,
            'sort_number' => 2,
            'step_group_id' => 2,
            'default_price_ht' => 0,
        ]);
        Step::create([
            'phase_id' => 1,
            'name' => 'Dossier complet',
            'is_visible' => 0,
            'sort_number' => 2,
            'step_group_id' => 2,
            'default_price_ht' => 0,
        ]);
        //groupe1 -> demande de piece complementaire / ou dossier complet

        //Groupe 1 -> delai de reponse : aucun


        Step::create([
            'phase_id' => 1,
            'name' => 'Vérification de la société, pas de procédure en cours',
            'is_visible' => 0,
            'sort_number' => 3,
            'step_group_id' => 3,
            'default_price_ht' => 0,

        ]);
        Step::create([
            'phase_id' => 1,
            'name' => 'Vérification de la société,procédure en cours ',
            'is_visible' => 0,
            'sort_number' => 3,
            'step_group_id' => 3,
            'default_price_ht' => 0,
        ]);
        //Groupe 2 -> delai de reponse : 1 semaine
        Step::create([
            'phase_id' => 2,
            'name' => 'Envoi d\'un courrier simple avec les pièces justificatives.',
            'is_visible' => 0,
            'sort_number' => 4,
            'step_group_id' => 4,
            'default_price_ht' => 0,
            'days_until_deadline' => 7
        ]); // -> on gerene un courier simple avec le recap du dossier 
        //Groupe 3 -> delai de reponse : 1 semaine
        Step::create([
            'phase_id' => 3,
            'name' => 'Appels téléphoniques.',
            'is_visible' => 0,
            'sort_number' => 5,
            'step_group_id' => 5,
            'default_price_ht' => 0,
            'days_until_deadline' => 7
        ]);
        //Groupe 4 -> delai de reponse : 8 jours
        Step::create([
            'phase_id' => 3,
            'name' => 'Mise en demeure (Scan LR/AR et courrier)',
            'is_visible' => 0,
            'sort_number' => 6,
            'step_group_id' => 6,
            'default_price_ht' => 0,
            'days_until_deadline' => 8
        ]);
        //Groupe 5 -> delai de reponse : aucun
        Step::create([
            'phase_id' => 4,
            'name' => 'Requête IP',
            'is_visible' => 0,
            'sort_number' => 7,
            'step_group_id' => 7,
            'default_price_ht' => 24000,
        ]); // pouvoir exporter tous les fichiers du dossier
        //Groupe 6 -> delai de reponse : aucun
        Step::create([
            'phase_id' => 4,
            'name' => 'Retour du greffe du dossier IP refusé',
            'is_visible' => 0,
            'sort_number' => 8,
            'step_group_id' => 8,
            'default_price_ht' => 0
        ]);
        Step::create([
            'phase_id' => 4,
            'name' => 'Retour du greffe du dossier IP accepté',
            'is_visible' => 0,
            'sort_number' => 8,
            'step_group_id' => 8,
            'default_price_ht' => 0
        ]);
        //Groupe 7 -> delai de reponse : aucun
        Step::create([
            'phase_id' => 4,
            'name' => 'Signification au débiteur',
            'is_visible' => 0,
            'step_group_id' => 9,
            'sort_number' => 9,
            'default_price_ht' => 0
        ]);
        //Groupe 8 -> delai de reponse :  1 mois
        Step::create([
            'phase_id' => 4,
            'name' => 'Retour acte signifié',
            'is_visible' => 0,
            'sort_number' => 10,
            'step_group_id' => 10,
            'default_price_ht' => 3000
        ]);
        //Groupe 9 -> delai de reponse : aucun
        Step::create([
            'phase_id' => 4,
            'name' => 'Le client fait opposition (Tribunal de Paris et Créance < 10 000€, OSF se rend à l\'audience)',
            'is_visible' => 0,
            'sort_number' => 11,
            'step_group_id' => 11,
            'default_price_ht' => 0,

        ]);
        Step::create([
            'phase_id' => 4,
            'name' => 'Le client fait opposition (Tribunal hors de Paris, OSF se rapproche du client)',
            'is_visible' => 0,
            'sort_number' => 11,
            'step_group_id' => 11,
            'default_price_ht' => 0,

        ]);
        Step::create([
            'phase_id' => 4,
            'name' => 'Le client ne fait pas opposition',
            'is_visible' => 0,
            'sort_number' => 11,
            'step_group_id' => 11,
            'default_price_ht' => 0,

        ]);

        //Groupe 12 -> delai de reponse : aucun
        Step::create([
            'phase_id' => 4,
            'name' => 'Envoi du titre au huissier',
            'is_visible' => 0,
            'sort_number' => 12,
            'step_group_id' => 12,
            'default_price_ht' => 0,

        ]);
        //Groupe 13 -> delai de reponse : aucun
        Step::create([
            'phase_id' => 4,
            'name' => 'Autre',
            'is_visible' => 0,
            'sort_number' => 13,
            'step_group_id' => 13,
            'default_price_ht' => 0,

        ]);
        // Apparait tout le temps
        /* Step::create([
            'phase_id' => 5,
            'name' => 'Le client a payé',
            'is_visible' => 0,
            'sort_number' => 0,
            'step_group_id' => 14,
            'default_price_ht' => 0,

        ]);
        Step::create([
            'phase_id' => 5,
            'name' => 'Arrêt des poursuites',
            'is_visible' => 0,
            'sort_number' => 0,
            'step_group_id' => 14,
            'default_price_ht' => 0,

        ]); */
        Step::create([
            'phase_id' => 5,
            'name' => 'Cloture du dossier',
            'is_visible' => 0,
            'sort_number' => 15,
            'step_group_id' => 15,
            'default_price_ht' => 0,

        ]);
        Step::create([
            'phase_id' => 6,
            'name' => 'Facturé',
            'is_visible' => 0,
            'sort_number' => 16,
            'step_group_id' => 15,
            'default_price_ht' => 0,

        ]);
    }
}
