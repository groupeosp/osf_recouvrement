<?php

namespace Database\Seeders;

use App\Enum\UserRoleEnum;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'client_id' => 1,
            'role' => UserRoleEnum::ADMIN,
            'first_name' => 'Laurent',
            'last_name' => 'Quenel',
            'email' => 'laurent-quenel@hotmail.Fr',
            'password' => Hash::make('Test1845')
        ]);
        User::create([
            'client_id' => 2,
            'role' => UserRoleEnum::USER,
            'first_name' => 'Client',
            'last_name' => 'Client',
            'email' => 'client@osp.Fr',
            'password' => Hash::make('Client1845')
        ]);
        User::create([
            'client_id' => 1,
            'role' => UserRoleEnum::ADMIN,
            'first_name' => 'Thomas',
            'last_name' => 'Gresle',
            'email' => 'tgresle@osp.fr',
            'password' => Hash::make('Thomas1845')
        ]);
        User::create([
            'client_id' => 1,
            'role' => UserRoleEnum::ADMIN,
            'first_name' => 'Dominique',
            'last_name' => 'Payen',
            'email' => 'dpayen@osp.fr',
            'password' => Hash::make('Compta1845')
        ]);
    }
}
