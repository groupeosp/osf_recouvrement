<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces informations ne coincident pas ...',
    'password' => 'Le mot de passe saisi est incorrect.',
    'throttle' => 'Trop d\'essai de connexion. Merci de ressayer dans :seconds seconds.',

];
