/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
import "v-calendar/dist/style.css";
import Vue from "vue";
import { SetupCalendar, Calendar, DatePicker } from "v-calendar";
import { createPinia } from "pinia";
import { createApp } from "vue";
import VueApexCharts from "vue3-apexcharts";
const pinia = createPinia();
const app = createApp({});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context("./", true, /\.vue$/i);
/* files.keys().map((key) => Vue.component(key.split("/").pop().split(".")[0], files(key).default)); */
files.keys().map((key) => app.component(key.split("/").pop().split(".")[0], files(key).default));

/* Vue.component("example-component", require("./components/ExampleComponent.vue").default); */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

app.use(pinia);
// Setup plugin for defaults or `$screens` (optional)
app.use(SetupCalendar, {});
app.use(VueApexCharts);
// Use the components
app.component("Calendar", Calendar);
app.component("DatePicker", DatePicker);
app.mount("#app");

/* const app = new Vue({
   el: "#app",
}); */
