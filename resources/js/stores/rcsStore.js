import { defineStore } from "pinia";

export const useRcsStore = defineStore("RcsStore", {
   state: () => {
      return {
         debtor: {},
      };
   },
   actions: {
      searchDebtor(rcs) {
         console.log(rcs);
      },
   },
});
