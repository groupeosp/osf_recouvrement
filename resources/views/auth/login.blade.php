@extends('layouts.auth')

@section('content')
<div class="container-auth">
    <div class="side-block">
     
    </div>
    <div class="main-block">
        <div class="log-container">

            <h1 class="flex items-center justify-center gap-2">Bonjour <emoji-component type="hello"></emoji-component>
            </h1>
            <h3>Bon retour parmi nous !</h3>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="input-form mb-3">
                    <label for="email" class="mylabel">Email</label>

                    <div class="">
                        <input id="email" type="email" class="myinput @error('email') is-invalid @enderror" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="input-form mb-3">
                    <label for="password" class="mylabel">Mot de passe</label>

                    <div class="">
                        <input id="password" type="password" class="myinput @error('password') is-invalid @enderror"
                            name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="flex flex-col">
                    @if (Route::has('password.request'))
                    <a class="forgot-password" href="{{ route('password.request') }}">
                        Mot de passe oublié ?
                    </a>
                    @endif

                    <button type="submit" class="btn btn-primary w-100">
                        Se connecter
                    </button>
                </div>

            </form>
        </div>
    </div>

</div>
<div class="logo_container">
    <img src="{{ asset('img/logo/logo_osfrecouvrement.svg') }}" alt="">
</div>
@endsection