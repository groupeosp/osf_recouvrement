@extends('layouts.auth')

@section('content')

<div class="container-auth">
    <div class="side-block">

    </div>
    <div class="main-block">
        <div class="log-container">

            <h1 class="flex items-center justify-center gap-2">Bienvenue <emoji-component type="hello">
                </emoji-component>
                <h3>Content de vous compter parmi nous !</h3>

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="input-form mb-3">
                        <label for="client" class="mylabel">Client</label>
                        <select class="myinput " name="client" id="client" required autofocus>
                            <option value="0" disabled selected></option>
                            @foreach ($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->name }}</option>
                            @endforeach
                        </select>
                        @error('client')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="input-form mb-3">
                        <label for="role" class="mylabel">Type d'utilisateur</label>
                        <select class="myinput " name="role" id="role" required autofocus>
                            <option value="0" disabled selected></option>
                            @foreach (App\Enum\UserRoleEnum::cases() as $enum)
                            <option value="{{ $enum->value }}">{{ $enum->value }}</option>
                            @endforeach
                        </select>
                        @error('role')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="input-form mb-3">
                        <label for="last_name" class="mylabel">Nom</label>

                        <div>
                            <input id="last_name" type="text"
                                class="myinput @error('nalast_nameme') is-invalid @enderror" name="last_name"
                                value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="input-form mb-3">
                        <label for="name" class="mylabel">Prénom</label>

                        <div>
                            <input id="first_name" type="text" class="myinput @error('first_name') is-invalid @enderror"
                                name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name"
                                autofocus>

                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="input-form mb-3">
                        <label for="email" class="mylabel">Email</label>

                        <div>
                            <input id="email" type="email" class="myinput @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="input-form mb-3">
                        <label for="password" class="mylabel">Mot de passe</label>

                        <div>
                            <input id="password" type="password" class="myinput @error('password') is-invalid @enderror"
                                name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="input-form mb-3">
                        <label for="password-confirm" class="mylabel">Confirmation mot de
                            passe</label>

                        <div>
                            <input id="password-confirm" type="password" class="myinput" name="password_confirmation"
                                required autocomplete="new-password">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary w-full">
                        Nous rejoindre
                    </button>
                </form>
        </div>
    </div>

</div>
<div class="logo_container">
    <img src="{{ asset('img/logo/logo_osfrecouvrement.svg') }}" alt="">
</div>
@endsection