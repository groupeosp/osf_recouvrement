@extends('layouts.app')

@section('content')
<client-page-component client_id="{{  Cookie::get('client_id')}}" user_role="{{ Auth::user()->role->value }}">
</client-page-component>
@endsection