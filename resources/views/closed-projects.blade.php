@extends('layouts.app')

@section('content')
<home-projects-component type="{{ App\Enum\ProjectStatusEnum::BILLING->value }}"
    client_id="{{ Cookie::get('client_id') }}" user_role="{{ Auth::user()->role->value }}">
</home-projects-component>
@endsection