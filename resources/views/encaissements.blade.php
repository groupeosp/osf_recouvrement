@extends('layouts.app')

@section('content')
<encaissement-page-component :is_admin="{{ Auth::user()->role->value == 'Admin'  }}"
    client_id="{{  Cookie::get('client_id')}}"></encaissement-page-component>
@endsection