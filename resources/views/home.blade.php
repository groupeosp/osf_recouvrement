@extends('layouts.app')

@section('content')

@if (session('success'))
<alert-component type="success"></alert-component>

@endif

<home-projects-component type="{{ App\Enum\ProjectStatusEnum::OPEN->value }}" client_id="{{  Cookie::get('client_id')}}"
    user_role="{{ Auth::user()->role->value }}">
</home-projects-component>
@endsection