<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>OSF - Génération courrier simple</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('img/logo/favicon.png') }}" type="image/x-icon">

    <!-- Styles -->
    {{--
    <link href="/css/pdf.css" rel="stylesheet" type="text/css" media="all"> --}}
    <style>
        @font-face {
            font-family: Roboto;
            src: url("../fonts/Roboto-Regular.ttf"); // remember that font file path must be relative to the current css file. not the final compiled folder
            font-weight: normal;
            font-style: normal;
        }

        ul {
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .printable {
            /*  height: 29.7cm;
            width: 21cm; */
            /*   border: 1px solid black; */
            font-size: 14px;
            color: #1a3463;
            font-family: Roboto, sans-serif
        }

        .printable__main {
            width: 100%;
            margin: 0;
            /*  padding: 10mm 15mm; */
        }

        .printable__main--header>.header {
            margin: 5mm auto;
        }

        .printable__main--header>.header>.header__address {
            /*  display: flex;
            display: -webkit-box;
            -webkit-box-pack: justify;
            justify-content: space-between;
            align-items: center; */
        }

        .printable__main--header>.header>.header__address>.bank {
            margin-top: 5mm;
            float: left;
            font-size: 14px;
            height: 35mm;
            width: 51%;
        }

        .printable__main--header>.header>.header__address>.bank>header {
            color: #286bee;
        }

        .printable__main--header>.header>.header__address>.client_address {
            float: right;
            padding: 5mm 10mm;
            width: 80mm;
            height: 25mm;
            border: 1px solid #7a868c;
            border-radius: 5mm;
        }

        .printable__main--header>.header>.header__address>.client_address>header {
            font-size: 16px;
            color: #286bee;
        }

        .printable__main--header>.footer {
            text-align: right;
        }

        .printable__main--body {
            text-align: justify;
        }

        .printable__main--body>p {
            margin: 5mm auto;
        }

        .printable__main--body .object {
            margin: 2mm auto;
        }

        .printable__main--body .dear {
            margin: 2mm auto;
        }

        .printable__main--body>section>section {
            width: 50mm;
        }

        .printable__main--body>section>section>p {
            text-align: center;
        }

        .printable__main--body>header>ul {
            font-weight: bold
        }

        .printable__main--footer {
            border-top: 1px solid #7a868c;
            margin: 5mm 0;
            padding: 2mm 0;
            text-align: center;

        }

        .printable__main--footer>p {
            font-size: 11px;
        }

        .header__logo>img {
            height: 8mm;
            margin-bottom: 10mm;
        }

        h5 {
            margin: 2mm 0;
            font-size: 14px;
            font-weight: bold;
            text-transform: uppercase;
            letter-spacing: 0.3mm;
        }

        p {
            margin: 1mm 0
        }

        table {
            width: 100%;
            border-collapse: collapse;
            /* border: 1px solid #7a868c; */
            margin: 8mm 0;
        }

        th {
            font-size: 12px;
            color: #7a868c;
            padding: 5px;
            font-weight: 100;
            text-align: left;
        }

        tbody>tr:nth-child(odd) {
            background-color: #F6F6F6
        }

        td {
            font-size: 14px;
            color: #1a3463;
            padding: 5px;
        }

        tr {
            padding: 7mm
        }

        .printable__main--footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0.1mm;
        }

        .total {
            font-weight: bold;
        }

        .right {
            text-align: right;
        }

        .signature {


            text-align: right;
            /*   padding: 0 10mm; */
        }

        @page {
            margin: 15mm 15mm 20mm !important;
            padding: 0 !important;
        }
    </style>
</head>

<body>
    <div id="app" class="printable lg:flex">
        <main class="printable__main">
            <header class="printable__main--header">
                <section class="header">
                    <section class="header__logo">
                        <img src="data:image/png;base64,PHN2ZyB3aWR0aD0iMTY4IiBoZWlnaHQ9IjE4IiB2aWV3Qm94PSIwIDAgMTY4IDE4IiBmaWxsPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8ZyBjbGlwLXBhdGg9InVybCgjY2xpcDBfODFfMzc3MSkiPgo8cGF0aCBkPSJNMjcuNTY4NyA3LjY5OTg3SDI1LjgwMTFWMTcuNjk3MkgyMy40Nzg2VjcuNjk5ODdIMjAuODgxN0wyMi42MzA4IDUuNTk4MjVIMjMuNDU1OUMyMy40NTU5IDMuODgyMjMgMjQuMDc0NyAyLjUwMzM2IDI1LjI2MjcgMS41MDIwMkMyNi40NTA4IDAuNTAwNjczIDI3Ljk1ODYgMCAyOS43OTg0IDBWMi4xMTk3OEMyNy4xMzM1IDIuMTE5NzggMjUuODAxMSAzLjI3MzIyIDI1LjgwMTEgNS41ODAwOEgyOC43NjcxTDI3LjU2ODcgNy42OTk4N1pNMjAuODA1NCAxMi4yODI2QzIwLjU0NDkgMTEuOTE1NSAyMC4xODE4IDExLjYyOTUgMTkuNzU5NyAxMS40NTlDMTkuMjk5IDExLjI1OTggMTguNTkyMiAxMS4wMDY3IDE3LjYzOTMgMTAuNjk5OUMxNi42ODQzIDEwLjQwMTEgMTYuMTM5OCAxMC4yMzM1IDE2LjAwMzcgMTAuMjAxMkwxNS40Njk0IDkuOTgzMThDMTUuMjg4IDkuODkyMjYgMTUuMTE2NiA5Ljc4MzM1IDE0Ljk1NzkgOS42NTgxNEMxNC44NDcyIDkuNTQ3MTggMTQuNzUyMiA5LjQyMjI1IDE0LjY3NTMgOS4yODY2OEMxNC41ODkyIDkuMTE2MDEgMTQuNTQ1NCA4LjkyNzg4IDE0LjU0NzQgOC43Mzc1NUMxNC41NDkzIDguNTcwODUgMTQuNjAxMiA4LjQwODM1IDE0LjY5NjUgOC4yNzAxOUMxNC43OTE4IDguMTMyMDMgMTQuOTI2NCA4LjAyNDI4IDE1LjA4MzcgNy45NjAzQzE1LjQyNDUgNy43OTk3OCAxNS43OTY4IDcuNzEzNjggMTYuMTc0OSA3LjcwNzk0SDE2LjM2MDVIMTkuODAwOUwyMS41NSA1LjYwNjMzSDE2LjM2MjZDMTUuMzI3NCA1LjU3NjYxIDE0LjMwODggNS44NjU0IDEzLjQ1MDEgNi40MzIwM0MxMi42NDk4IDYuOTY3MDMgMTIuMjQ5NyA3LjcyNDA5IDEyLjI0OTcgOC43NDE1OUMxMi4yMjgzIDkuNDczMDkgMTIuNDcyMyAxMC4xODg0IDEyLjkzODYgMTAuNzYwNEMxMy4zOTk5IDExLjI4MzMgMTMuOTg1OCAxMS42ODY5IDE0LjY0NDQgMTEuOTM1NEMxNS4zMjUxIDEyLjE2ODIgMTUuOTk3NSAxMi4zNzYyIDE2LjY2MTYgMTIuNTU5MkMxNy4yNjAzIDEyLjcwODcgMTcuODM2IDEyLjkzNTUgMTguMzczNiAxMy4yMzM1QzE4Ljg1MDggMTMuNTAyNyAxOS4wODg2IDEzLjgzOTIgMTkuMDg3MyAxNC4yNDI5QzE5LjA4NzMgMTQuMjc1MiAxOS4wODczIDE0LjMwNzUgMTkuMDg3MyAxNC4zMzk4QzE5LjAxNzEgMTUuMTI1MiAxOC4yNDk4IDE1LjUyNjkgMTYuNjkwNSAxNS41MjY5SDEyLjM3NTVDMTEuOTQ2NSAxNi4yMzU1IDExLjM0NDIgMTYuODc3NSAxMC4yNjEzIDE3LjU5MjJIMTYuNjk0NkMxNi45MDA5IDE3LjQ0MDggMTYuOTAwOSAxNy40NDI4IDE2LjY5NDYgMTcuNTkyMkMxOS44MjcxIDE3LjU5MjIgMjEuMzkzMyAxNi40NzUxIDIxLjM5MzMgMTQuMjQwOUMyMS4zOTI3IDEzLjkyMjEgMjEuMzU4OSAxMy42MDQxIDIxLjI5MjIgMTMuMjkyMUMyMS4yMDk1IDEyLjkyNDEgMjEuMDQzMSAxMi41NzkgMjAuODA1NCAxMi4yODI2Wk0xMS45ODc3IDE0LjA0OTFDMTIuMzE0NCAxMy4yODE4IDEyLjQ3OTIgMTIuNDU3OCAxMi40NzI1IDExLjYyNjVDMTEuODczNiAxMS4wNjQ5IDExLjQ4MzIgMTAuMzI0IDExLjM2MjggOS41MjA4NkMxMS4yOTkyIDkuMDQwMjMgMTEuMzA5NiA4LjU1Mjk3IDExLjM5MzcgOC4wNzUzN0MxMS4xNzI1IDcuNzQ5IDEwLjkxODIgNy40NDUzMSAxMC42MzQ3IDcuMTY4OTFDMTAuMDY3NSA2LjYwNDE2IDkuMzg5ODcgNi4xNTcxNiA4LjY0MjE4IDUuODU0NjRDNy44OTQzMiA1LjUxOTIyIDcuMDgwNDIgNS4zNDgzMiA2LjI1NzggNS4zNTM5N0M1LjQyMzg3IDUuMzMzMDcgNC41OTQ4MSA1LjQ4NDQ0IDMuODI0NjcgNS43OTgyQzMuMDU0NTIgNi4xMTE5NyAyLjM2MDU5IDYuNTgxMDggMS43ODgxMSA3LjE3NDk3QzAuNTk1OTIgOC4zNTgwMSAtMC4wMDAxNzU3MzEgOS44MzMxMSAtMC4wMDAxNzU3MzEgMTEuNjAwM0MtMC4wMDgzNzgxMiAxMi40Mjg2IDAuMTQ0OTMzIDEzLjI1MDggMC40NTE1MzcgMTQuMDIyOUMwLjc2NDI5NiAxNC43NTY1IDEuMjE0ODYgMTUuNDI2MiAxLjc3OTg2IDE1Ljk5NzNDMi4zNDMyMSAxNi41NjQ2IDMuMDEwNzggMTcuMDIyOCAzLjc0NzYgMTcuMzQ3OUM0LjU0MDMzIDE3LjY2OTcgNS4zOTE0IDE3LjgzMTEgNi4yNDk1NSAxNy44MjIzQzcuMDk4ODIgMTcuODI5IDcuOTQwNyAxNy42Njc2IDguNzI0NjkgMTcuMzQ3OUM5LjQ3NDg4IDE3LjAzMDEgMTAuMTU0NSAxNi41NzIgMTAuNzI1NCAxNS45OTkzQzExLjI3NSAxNS40MzgzIDExLjcwNDIgMTQuNzc1MiAxMS45ODc3IDE0LjA0OTFaTTYuNzc5NjQgMTUuNzIwN0M3LjA3NjY2IDE1LjY5ODUgNy4zNjMzNiAxNS42NjgyIDcuNjUwMDYgMTUuNjE5OEM3LjM2MzM2IDE1LjY2ODIgNy4wNzQ1OSAxNS42OTg1IDYuNzc5NjQgMTUuNzIwN1pNOS4xMDIxNSAxNC42MDAzQzguNDc5NzggMTUuMjYzMSA3LjYyMDk3IDE1LjY2NjQgNi43MDMzMiAxNS43MjY4QzYuNTQ2NTYgMTUuNzI2OCA2LjM4NTY4IDE1Ljc0NSA2LjIxODYxIDE1Ljc0OUM1LjAyNjQyIDE1Ljc0OSA0LjA2Mzg2IDE1LjM2NTQgMy4zMzA5NSAxNC41OTgzQzIuNTk4MDMgMTMuODMxMSAyLjI0Mzk1IDEyLjgzMTggMi4yNjg3IDExLjYwMDNDMi4yNjg3IDEwLjM2ODggMi42MTcyOCA5LjU4MTQzIDMuMzc2MzMgOC42MDIyOUMyLjY0NzUzIDkuNTQ0NDEgMi42NDc1MyA5LjU0NDQxIDMuMzc2MzMgOC42MDIyOUM0LjA5MjA1IDcuNzYyNDUgNS4wNjk3MyA3LjQ1MzU3IDYuMjYzOTkgNy40NTM1N0M3LjQ1ODI0IDcuNDUzNTcgOC40MzU5MiA3Ljg0NTIyIDkuMTUxNjUgOC42MjY1MUM5LjY1OTQ5IDkuMTgyMTUgOS45OTc1NiA5Ljg2NjMxIDEwLjEyNzMgMTAuNjAwOUMxMC4xNTYxIDEwLjkxNzkgMTAuMTcyNiAxMS4yNDkgMTAuMTgzIDExLjYxMDRDMTAuMTc3NSAxMi44MjE3IDkuODE3MTggMTMuODE4MyA5LjEwMjE1IDE0LjYwMDNaIiBmaWxsPSIjMjg2QkVFIi8+CjxwYXRoIGQ9Ik0zNC44NzY5IDUuNTcyMDJDMzcuOTUyMyA1LjU3MjAyIDM5LjQ4ODkgNy4zMDQxOSAzOS40ODg5IDkuNDQwMTJDMzkuNDg4OSAxMC45ODA1IDM4LjYzMDkgMTIuNDM0MSAzNi43MDAzIDEyLjk5MzNMMzkuNTg3OSAxNy44NTg3SDM2LjE0NTRMMzMuNTM0MSAxMy4yMTU0SDMyLjc5NTdWMTcuODU4N0gyOS43NDUxVjcuNjk5ODhMMzAuODk0IDUuNTcyMDJIMzQuODc2OVpNMzQuNjk3NSA4LjA1NzIySDMyLjc5NTdWMTEuMTAzNkgzNC42OTEzQzM1LjgzNiAxMS4xMDM2IDM2LjM3MjMgMTAuNTI2MyAzNi4zNzIzIDkuNTYzMjdDMzYuMzc4NSA4LjY1Mjc3IDM1Ljg0MjIgOC4wNTcyMiAzNC42OTc1IDguMDU3MjJaIiBmaWxsPSIjMTAyMzQ3Ii8+CjxwYXRoIGQ9Ik00NS44NjQ0IDE3Ljk5OTlDNDIuOTMxMyAxNy45OTk5IDQwLjgyMTMgMTYuMDc0IDQwLjgyMTMgMTIuOTc1QzQwLjgyMTMgOS44NzYxMSA0Mi45MDI1IDcuOTUyMTUgNDUuODY0NCA3Ljk1MjE1QzQ4Ljc3ODkgNy45NTIxNSA1MC44MzUzIDkuODQxNzkgNTAuODM1MyAxMi44MTc2QzUwLjgzNDQgMTMuMTEwNCA1MC44MTY1IDEzLjQwMjkgNTAuNzgxNyAxMy42OTM3SDQzLjg2MTZDNDMuOTY4OCAxNC45NzE3IDQ0Ljc3MzMgMTUuNTY3MiA0NS43NTcxIDE1LjU2NzJDNDYuMDg5NCAxNS41ODc4IDQ2LjQyMDEgMTUuNTA4IDQ2LjcwNDUgMTUuMzM4N0M0Ni45ODg5IDE1LjE2OTQgNDcuMjEzMyAxNC45MTg2IDQ3LjM0NzQgMTQuNjIwNEg1MC42MDIyQzUwLjEyMTYgMTYuNTI4MiA0OC4zNDk4IDE3Ljk5OTkgNDUuODY0NCAxNy45OTk5Wk00My44NzgxIDEyLjAzMDJINDcuNzA0MkM0Ny43MDQyIDEwLjk2MjIgNDYuODQ2MiAxMC4zNTA1IDQ1LjgyNzMgMTAuMzUwNUM0NC44MDgzIDEwLjM1MDUgNDQuMDU3NSAxMC45NDYxIDQzLjg3ODEgMTIuMDMwMloiIGZpbGw9IiMxMDIzNDciLz4KPHBhdGggZD0iTTU2Ljg0MTQgNy45NTIxNUM1OS4zNDU0IDcuOTUyMTUgNjEuMTE1MiA5LjMwMDc0IDYxLjYzMjkgMTEuNjI4NUg1OC4zNzE5QzU4LjEyMjMgMTAuOTQ2MSA1Ny42MjExIDEwLjUwOCA1Ni43ODE2IDEwLjUwOEM1NS42OTA1IDEwLjUwOCA1NC45Mzk3IDExLjM0NzggNTQuOTM5NyAxMi45NzVDNTQuOTM5NyAxNC42MDIyIDU1LjY5MDUgMTUuNDQ0MSA1Ni43ODc4IDE1LjQ0NDFDNTcuNjI3MyAxNS40NDQxIDU4LjEwOTkgMTUuMDQwMyA1OC4zNzgxIDE0LjMyMzZINjEuNjMyOUM2MS4xMTUyIDE2LjU4MDcgNTkuMzQ1NCAxNy45OTk5IDU2Ljg0MTQgMTcuOTk5OUM1My45MDg0IDE3Ljk5OTkgNTEuODM1NCAxNi4wNzQgNTEuODM1NCAxMi45NzVDNTEuODM1NCA5Ljg3NjExIDUzLjkwODQgNy45NTIxNSA1Ni44NDE0IDcuOTUyMTVaIiBmaWxsPSIjMTAyMzQ3Ii8+CjxwYXRoIGQ9Ik02Ny44MDIgMTcuOTk5OUM2NC44NjkgMTcuOTk5OSA2Mi42NDU1IDE2LjA3NCA2Mi42NDU1IDEyLjk3NUM2Mi42NDU1IDkuODc2MTEgNjQuOTE0NCA3Ljk1MjE1IDY3LjgzMDkgNy45NTIxNUM3MC43NDc1IDcuOTUyMTUgNzMuMDE2MyA5Ljg3ODEyIDczLjAxNjMgMTIuOTc1QzczLjAxNjMgMTYuMDcxOSA3MC43MzMgMTcuOTk5OSA2Ny44MDIgMTcuOTk5OVpNNjcuODAyIDE1LjQwOThDNjguODkxMSAxNS40MDk4IDY5LjkyMjQgMTQuNjIwNCA2OS45MjI0IDEyLjk3NUM2OS45MjI0IDExLjMyOTcgNjguOTIgMTAuNTUyNCA2Ny44NDc0IDEwLjU1MjRDNjYuNzM3NyAxMC41NTI0IDY1Ljc3MjQgMTEuMzIzNiA2NS43NzI0IDEyLjk3NUM2NS43NzI0IDE0LjYyNjQgNjYuNjkyNCAxNS40MDk4IDY3LjgwMiAxNS40MDk4WiIgZmlsbD0iIzEwMjM0NyIvPgo8cGF0aCBkPSJNODQuNTAxNCAxNy44NTg3SDgxLjQ0MjVWMTYuNTI4M0M4MS4wOTQ5IDE2Ljk4OTEgODAuNjM4NSAxNy4zNjA3IDgwLjExMjMgMTcuNjExM0M3OS41ODYxIDE3Ljg2MiA3OS4wMDU3IDE3Ljk4NDMgNzguNDIwOCAxNy45Njc3Qzc2LjExNDggMTcuOTY3NyA3NC41NTk2IDE2LjQxMTIgNzQuNTU5NiAxMy44MDI5VjguMDkxNTVINzcuNTk5OVYxMy4zOTUxQzc3LjU5OTkgMTQuNzI1NSA3OC4zNjcyIDE1LjQ2MDMgNzkuNTExOSAxNS40NjAzQzgwLjY5MTcgMTUuNDYwMyA4MS40NDI1IDE0LjcyNTUgODEuNDQyNSAxMy4zOTUxVjguMDkxNTVIODQuNTAxNFYxNy44NTg3WiIgZmlsbD0iIzEwMjM0NyIvPgo8cGF0aCBkPSJNODkuMDI0MyA4LjA5MTU1TDkxLjE4OCAxNS4wNTg2TDkzLjM1MTcgOC4wOTE1NUg5Ni42MDQ1TDkzLjA2NSAxNy44NTg3SDg5LjI5MjVMODUuNzUxIDguMDkxNTVIODkuMDI0M1oiIGZpbGw9IiMxMDIzNDciLz4KPHBhdGggZD0iTTEwMC45NDggMTcuODU4Nkg5Ny44OTE2VjguMDkxNDNIMTAwLjk0OFY5LjcyMDY0QzEwMS42NDggOC42ODY5OSAxMDIuNzM3IDcuOTg2NDUgMTA0LjA3OSA3Ljk4NjQ1VjExLjE1NkgxMDMuMjM4QzEwMS43OTQgMTEuMTU2IDEwMC45NDggMTEuNjQ0NiAxMDAuOTQ4IDEzLjMyNjNWMTcuODU4NloiIGZpbGw9IiMxMDIzNDciLz4KPHBhdGggZD0iTTEwOS45NzkgMTcuOTk5OUMxMDcuMDQ2IDE3Ljk5OTkgMTA0LjkzNiAxNi4wNzQgMTA0LjkzNiAxMi45NzVDMTA0LjkzNiA5Ljg3NjExIDEwNy4wMTEgNy45NTIxNSAxMDkuOTc5IDcuOTUyMTVDMTEyLjg5MyA3Ljk1MjE1IDExNC45NSA5Ljg0MTc5IDExNC45NSAxMi44MTc2QzExNC45NDkgMTMuMTEwNCAxMTQuOTMxIDEzLjQwMjkgMTE0Ljg5NiAxMy42OTM3SDEwNy45NzZDMTA4LjA4MyAxNC45NzE3IDEwOC44ODggMTUuNTY3MiAxMDkuODcxIDE1LjU2NzJDMTEwLjIwNCAxNS41ODc2IDExMC41MzUgMTUuNTA3OCAxMTAuODE5IDE1LjMzODVDMTExLjEwNCAxNS4xNjkyIDExMS4zMjkgMTQuOTE4NiAxMTEuNDY0IDE0LjYyMDRIMTE0LjcxNkMxMTQuMjM0IDE2LjUyODIgMTEyLjQ2NCAxNy45OTk5IDEwOS45NzkgMTcuOTk5OVpNMTA3Ljk5NCAxMi4wMzAySDExMS44MjFDMTExLjgyMSAxMC45NjIyIDExMC45NjMgMTAuMzUwNSAxMDkuOTQ0IDEwLjM1MDVDMTA4LjkyNSAxMC4zNTA1IDEwOC4xNzIgMTAuOTQ2MSAxMDcuOTk0IDEyLjAzMDJaIiBmaWxsPSIjMTAyMzQ3Ii8+CjxwYXRoIGQ9Ik0xMzAuMzA4IDEyLjU1NTFDMTMwLjMwOCAxMS4yNzcyIDEyOS41NTcgMTAuNTc4NiAxMjguMzk1IDEwLjU3ODZDMTI3LjIzNCAxMC41Nzg2IDEyNi40ODEgMTEuMjc3MiAxMjYuNDgxIDEyLjU1NTFWMTcuODU4NkgxMjMuNDQxVjEyLjU1NTFDMTIzLjQ0MSAxMS4yNzcyIDEyMi42OSAxMC41Nzg2IDEyMS41MjkgMTAuNTc4NkMxMjAuMzY4IDEwLjU3ODYgMTE5LjYxNSAxMS4yNzcyIDExOS42MTUgMTIuNTU1MVYxNy44NTg2SDExNi41NThWOC4wOTE0M0gxMTkuNjE1VjkuMzE2ODdDMTIwLjIwNSA4LjUyOTUyIDEyMS4yMjQgNy45ODY0NSAxMjIuNTI5IDcuOTg2NDVDMTI0LjAzMSA3Ljk4NjQ1IDEyNS4yNDggOC42MzQ1IDEyNS45MjcgOS44MDM0MUMxMjYuMjg3IDkuMjQ5OTIgMTI2Ljc4NCA4Ljc5MzY4IDEyNy4zNzEgOC40NzYzMUMxMjcuOTU5IDguMTU4OTMgMTI4LjYxOCA3Ljk5MDUzIDEyOS4yODkgNy45ODY0NUMxMzEuNzY0IDcuOTg2NDUgMTMzLjM0OCA5LjUyNjgzIDEzMy4zNDggMTIuMTUzM1YxNy44NTg2SDEzMC4zMDhWMTIuNTU1MVoiIGZpbGw9IiMxMDIzNDciLz4KPHBhdGggZD0iTTEzOS45MjYgMTcuOTk5OUMxMzYuOTk1IDE3Ljk5OTkgMTM0Ljg4NSAxNi4wNzQgMTM0Ljg4NSAxMi45NzVDMTM0Ljg4NSA5Ljg3NjExIDEzNi45NDcgNy45NTIxNSAxMzkuOTI2IDcuOTUyMTVDMTQyLjg0IDcuOTUyMTUgMTQ0Ljg5NyA5Ljg0MTc5IDE0NC44OTcgMTIuODE3NkMxNDQuODk3IDEzLjExMDQgMTQ0Ljg3OSAxMy40MDMgMTQ0Ljg0MyAxMy42OTM3SDEzNy45MjNDMTM4LjAzIDE0Ljk3MTcgMTM4LjgzNyAxNS41NjcyIDEzOS44MTkgMTUuNTY3MkMxNDAuMTUxIDE1LjU4NzYgMTQwLjQ4MiAxNS41MDc4IDE0MC43NjcgMTUuMzM4NUMxNDEuMDUxIDE1LjE2OTIgMTQxLjI3NiAxNC45MTg2IDE0MS40MTEgMTQuNjIwNEgxNDQuNjY2QzE0NC4xNzcgMTYuNTI4MiAxNDIuNDExIDE3Ljk5OTkgMTM5LjkyNiAxNy45OTk5Wk0xMzcuOTQyIDEyLjAzMDJIMTQxLjc2OEMxNDEuNzY4IDEwLjk2MjIgMTQwLjkxIDEwLjM1MDUgMTM5Ljg5MSAxMC4zNTA1QzEzOC44NzIgMTAuMzUwNSAxMzguMTIxIDEwLjk0NjEgMTM3Ljk0MiAxMi4wMzAyWiIgZmlsbD0iIzEwMjM0NyIvPgo8cGF0aCBkPSJNMTUzLjM5IDEyLjU1NTFDMTUzLjM5IDExLjIyNDcgMTUyLjYzMyAxMC40OTc5IDE1MS40NzYgMTAuNDk3OUMxNTAuMzE5IDEwLjQ5NzkgMTQ5LjU2MiAxMS4yMzI3IDE0OS41NjIgMTIuNTYzMlYxNy44NTg2SDE0Ni41MDVWOC4wOTE0M0gxNDkuNTYyVjkuMzg3NTNDMTUwLjE3MSA4LjU2Mzg0IDE1MS4yNDMgNy45ODY0NSAxNTIuNTg2IDcuOTg2NDVDMTU0Ljg5MiA3Ljk4NjQ1IDE1Ni40MjkgOS41MjY4MyAxNTYuNDI5IDEyLjE1MzNWMTcuODU4NkgxNTMuMzlWMTIuNTU1MVoiIGZpbGw9IiMxMDIzNDciLz4KPHBhdGggZD0iTTE2NC4wOTcgMTAuNTc4N1Y4LjA5MTUySDE2Mi4wOTdWNS43MTEzSDE1OS4wMzhWOC4wOTE1MkgxNTcuODIzVjEwLjU3ODdIMTU5LjAzOFYxNC40NjNDMTU5LjAzOCAxNi45MzQgMTYwLjQwOSAxNy44MTIyIDE2Mi4zOTggMTcuODU2N0wxNjMuODYgMTUuMzIxSDE2My4wMzVDMTYyLjM1NiAxNS4zMjEgMTYyLjEwNyAxNS4wNzY3IDE2Mi4xMDcgMTQuNDk5M1YxMC41Nzg3SDE2NC4wOTdaIiBmaWxsPSIjMTAyMzQ3Ii8+CjxwYXRoIGQ9Ik0xNjggMTUuMzIwOUwxNjYuNTM2IDE3Ljg1NjZIMTYzLjY5NUwxNjUuMTU4IDE1LjMyMDlIMTY4WiIgZmlsbD0iIzEwMjM0NyIvPgo8L2c+CjxkZWZzPgo8Y2xpcFBhdGggaWQ9ImNsaXAwXzgxXzM3NzEiPgo8cmVjdCB3aWR0aD0iMTY4IiBoZWlnaHQ9IjE4IiBmaWxsPSJ3aGl0ZSIvPgo8L2NsaXBQYXRoPgo8L2RlZnM+Cjwvc3ZnPgo="
                            alt="OSP logo">
                        <p>
                            <icon-component type="phone" color="myprimary" size="16"></icon-component> 01 49 04 01 58
                        </p>
                        <p>
                            <icon-component type="mail" color="myprimary" size="16"></icon-component>
                            recouvrement@osf-recouvrement.fr
                        </p>
                    </section>
                    <section class="header__address">
                        <article class="bank">
                            <header>
                                <h5>Nos coordonnées bancaires :</h5>
                            </header>
                            <ul>
                                <li><b>Société Générale</b></li>
                                <li><b>IBAN:</b> FR76 3000 6036 2000 0200 2303 069</li>
                                <li><b>BIC:</b> SOGEFRPP</li>
                            </ul>
                        </article>
                        <article class="client_address">
                            <header>
                                <h5>
                                    {{ $debtor['name'] }}
                                </h5>
                            </header>
                            <address>{!! $debtor['address'] !!}</address>
                        </article>
                    </section>
                </section>
                <section class="footer">
                    A <b>Neuilly-sur-Seine</b>, le <span class="date">{{date_format( now(),"d/m/Y"); }}</span>
                </section>
            </header>
            <section class="printable__main--body">
                <header>
                    <ul>
                        @if (isset($ndoss))
                        <li>N. doss : {{ $ndoss }}</li>
                        @endif
                        @if (isset($nref))
                        <li>N. réf : {{ $nref }}</li>
                        @endif
                    </ul>
                    <p class="object">Objet : <b>{{$object}}</b></p>
                    <p class="dear">Madame, Monsieur,</p>
                </header>
                <section class="main_body">
                    @yield('content')
                </section>
                <footer>
                    <p>Nous vous prions d'agréer, Madame, Monsieur, l'expression de nos salutations.</p>
                    <section class="signature">
                        <section>
                            <p>Service Recouvrement<br>Mme. Dominique PAYEN</p>
                        </section>
                    </section>
                </footer>
            </section>

        </main>
    </div>
    <script type="text/php">
        if ( isset($pdf) ) { 
            $pdf->page_script('
                if ($PAGE_COUNT > 1) {
                    
                    $font = $fontMetrics->get_font("Roboto, sans-serif", "normal");
                    $size = 8;
                    $footer1 = "OSF Recouvrement - CS 30018 - 14 RUE BEFFROY - 92523 Neuilly-sur-Seine cedex";
                    $footer2 = "SARL au capital de 5 000E / Siret : 531 225 266 000 26 / RCS NANTERRE / APE 7311Z / N°TVA: FR51 531225266";
                    $y = $pdf->get_height() - $size - 30;
                    $x = $pdf->get_width() - 450 ;
                   
                    $pdf->text($x, $y, $footer1, $font, $size , array(26/255,52/255,99/255));
                    $pdf->text($x-50, $y+10, $footer2, $font, $size , array(26/255,52/255,99/255));
                } 
            ');
           
        }
        </script>
</body>

</html>