<header class="py-4 flex flex-col gap-3 lg:gap-2">
    <section class="lg:hidden">
        <article class="w-full flex justify-between items-center">
            <img src="{{ asset('img/logo/logo_osfrecouvrement.svg') }}" alt="logo osf recouvrement">
            <article>
                <button class="btn !p-0 w-10" onclick="myOpen();">
                    <icon-component type="menu"></icon-component>
                </button>
            </article>
        </article>
    </section>
    <section>
        <article class="w-full flex justify-between items-center">
            <div class="flex items-center gap-2">
                <emoji-component type="hello"></emoji-component>
                <h5>
                    <span>Bonjour <b>{{ Auth::user()->first_name }}</b> !</span>
                </h5>
            </div>
            <article class="gap-2 flex">

                <a class="hidden lg:block btn btn-primary no-underline hover:font-semibold "
                    href="{{ route('new-project') }}">
                    <span class="flex justify-center gap-2">
                        <icon-component type="folder_plus" color='white'></icon-component>
                        Nouveau Dossier
                    </span>


                </a>
                <header-profile-component :user="{{ Auth::user()}}" :notification_count="2"></header-profile-component>

                {{-- <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button class="btn">
                        <icon-component type="logout"></icon-component>
                    </button>
                </form> --}}
            </article>
        </article>
    </section>
</header>