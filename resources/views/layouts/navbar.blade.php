<div id="back-block" class="fixed inset-0 bg-black opacity-40 z-10 "></div>
<nav id="side-block" class="p-10 h-screen fixed bg-white z-20 w-300 lg:fixed border-r">
    <section class="flex justify-between items-center py-4">
        <article class="w-56 h-8 flex items-center justify-center "><img class="hidden lg:block scale-125"
                src="{{ asset('img/logo/logo_osfrecouvrement.svg') }}" alt="Logo OSF recouvrement">
        </article>
        <button class="btn lg:hidden" onclick="myClose();">
            <icon-component type='close'></icon-component>
        </button>
    </section>
    <section class="flex flex-col justify-between py-10 h-5/6">
        <ul class="py-0">
            <li class="py-2 flex gap-1 items-center justify-between w-56">
                <div class="w-full lg:w-48 flex items-center">




                    <div class="w-full flex-1 flex items-center gap-3">

                        @if (Auth::user()->role != App\Enum\UserRoleEnum::USER)
                        <div class="w-full relative text-gray-600">
                            <span class="absolute inset-y-0 left-0 flex items-center pl-1">
                                <div class="p-1 focus:outline-none focus:shadow-outline">
                                    <icon-component type="user"></icon-component>
                                </div>
                            </span>


                            <form name="myform" action="{{ route('client.change') }}" method="post">
                                @csrf
                                <select class="text-xl font-semibold  py-2 pl-10 w-full border-0 focus:outline-none"
                                    name="client_id" value="{{ Cookie::get('client_id')}}" id="client_view"
                                    onchange="this.form.submit()">
                                    @foreach ($clients as $client)
                                    <option class="px-2 py-1" value="{{ $client->id }}">{{ strtoupper($client->name) }}
                                    </option>
                                    @endforeach
                                </select>
                            </form>
                        </div>
                        @else
                        <div class="p-1 focus:outline-none focus:shadow-outline">
                            <icon-component type="user"></icon-component>
                        </div>
                        <div>
                            <p class="text-xl font-semibold">{{ $clients[Cookie::get('client_id')-1]->name }}</p>

                        </div>
                        @endif
                    </div>
                </div>
                <div>
                    @if (Auth::user()->role != App\Enum\UserRoleEnum::USER)
                    <a class="no-underline  hover:font-semibold hover:text-myblack " href="{{ route('client') }}">
                        <icon-component type='right' size="16">
                    </a>
                    @endif
                </div>
            </li>
            <li class="py-2 flex gap-1 items-center justify-between w-56">
                <div
                    class="px-2 py-1 w-48 flex gap-3 items-center @if (request()->routeIs('home')) bg-myprimary rounded text-mywhite @endif">
                    <icon-component type='project_open' @if (request()->routeIs('home')) color="white" @endif>
                    </icon-component><a
                        class="no-underline  hover:font-semibold hover:text-myblack  @if (request()->routeIs('home'))hover:font-normal text-mywhite hover:text-mywhite  @else text-myblack   @endif"
                        href="{{ route('home') }}">
                        Dossiers en cours
                    </a>
                </div>
                <div>
                    <icon-component type='right' size="16">
                </div>
            </li>

            @if (Cookie::get('project_id') != null)
            <li class="py-2 flex gap-1 items-center justify-between w-56">
                <div
                    class="px-2 py-1 w-48 flex gap-3 items-center @if (request()->routeIs('project')) bg-myprimary rounded text-mywhite @endif">
                    <icon-component type='sub_arrow' @if (request()->routeIs('project'))
                        color="white" @endif>
                    </icon-component><a
                        class="no-underline  hover:font-semibold hover:text-myblack  @if (request()->routeIs('project') )hover:font-normal text-mywhite hover:text-mywhite  @else text-myblack   @endif"
                        href="{{ route('project', request()->project_id ? request()->project_id : Cookie::get('project_id') ) }}">
                        Dossiers n° {{ request()->project_id ? request()->project_id : Cookie::get('project_id')
                        }}
                    </a>
                </div>
                <div>
                    <icon-component type='right' size="16">
                </div>
            </li>
            @endif
            <li class="py-2 flex gap-1 items-center justify-between w-56">
                <div
                    class="px-2 py-1 w-48 flex gap-3 items-center @if (request()->routeIs('projects-page.closed')) bg-myprimary rounded text-mywhite @endif">
                    <icon-component type='project_close' @if (request()->routeIs('projects-page.closed')) color="white"
                        @endif>
                    </icon-component><a
                        class="no-underline  hover:font-semibold hover:text-myblack  @if (request()->routeIs('projects-page.closed')) hover:font-normal text-mywhite hover:text-mywhite  @else text-myblack   @endif"
                        href="{{ route('projects-page.closed') }}">
                        Dossiers Facturés
                    </a>
                </div>
                <div>
                    <icon-component type='right' size="16">
                </div>
            </li>
            @if (Auth::user()->role != App\Enum\UserRoleEnum::USER)
            <li class="py-2 flex gap-1 items-center justify-between  w-56">
                <div
                    class="px-2 py-1 w-48 flex gap-3 items-center @if (request()->routeIs('encaissements')) bg-myprimary rounded text-mywhite @endif">
                    <icon-component type='encaissement' @if (request()->routeIs('encaissements')) color="white" @endif>
                    </icon-component><a
                        class="no-underline hover:font-semibold hover:text-myblack  @if (request()->routeIs('encaissements'))hover:font-normal text-mywhite hover:text-mywhite  @else text-myblack   @endif"
                        href="{{ route('encaissements', ['client_id' =>  isset($_GET['client_id']) ? $_GET['client_id']: Cookie::get('client_id')]) }}">
                        Encaissements
                    </a>
                </div>
                <div>
                    <icon-component type='right' size="16">
                </div>
            </li>
            @endif
        </ul>
        <ul class="py-10 lg:hidden">
            @if (Auth::check())
            <li class="py-2 flex gap-1 items-center justify-center w-56">
                <form class="flex gap-2 items-center" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <icon-component type='logout' color="darkgrey" size="16"></icon-component><button
                        class="text-sm hover:underline hover:font-normal hover:text-myblack no-underline ">
                        Se déconnecter
                    </button>
                </form>

            </li>
            @endif
        </ul>
    </section>
</nav>