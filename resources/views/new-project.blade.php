@extends('layouts.app')

@section('content')

<new-project-page-component
    :client_id="{{ Cookie::get('client_id') ? Cookie::get('client_id') : Auth::user()->client_id }}"
    :is_admin="{{ Auth::user()->role->value == 'Admin'  }}">
</new-project-page-component>
@endsection