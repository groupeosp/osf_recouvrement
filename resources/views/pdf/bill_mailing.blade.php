@extends('layouts.bill',['client' => $client,'cegid_id' => $cegid_id,'bill_id' => $bill_id])


@prepend('styles')
<style>

</style>
@endprepend

@section('content')
@if ($projects)
<table class="align-top">
    <thead>
        <th>N° Dossier</th>
        <th>Débiteur</th>
        <th>Frais engagés (HT)</th>
        <th>Total F.engagés (HT)</th>
        <th>Frais de dossier (HT)</th>
        <th>Total F.dossier (HT)</th>
        <th>Total (HT)</th>
    </thead>
    <tbody>
        @foreach($projects as $project)
        <tr>
            <td class="bold primary">N°{{ sprintf("%06d",$project->id )}}</td>
            <td>{{ strtoupper($project->debtor->last_name) }}</td>
            <td>

                @if (count($project->engagedFees) > 0)
                <table class="sub-table">
                    @foreach($project->engagedFees as $engagedFee)
                    <tr>
                        <td>•</td>
                        <td>{{ $engagedFee->name }}</td>
                        <td class="right">{{ number_format($engagedFee->amount_ht/ 100,2) }}€</td>
                    </tr>
                    @endforeach
                </table>
                @else
                Aucun frais engagés
                @endif

            </td>
            <td class="bold">{{ number_format($project->totalEngagedFeesHT / 100,2) }}€</td>
            <td>
                <table class="sub-table">
                    @if (count($project->steps) > 0)
                    @foreach($project->steps as $step)
                    @if ($step->default_price_ht > 0)
                    <tr class="tiny">
                        <td>•</td>
                        <td>{{ $step->name }}</td>
                        <td class="right">{{ number_format($step->default_price_ht / 100,2) }}€</td>
                    </tr>
                    @endif
                    @endforeach
                    <tr class="tiny">
                        <td>•</td>
                        <td>Total commission</td>
                        <td class="right">{{ number_format(($project->totalCommissionHT)/100,2) }}€</td>
                    </tr>
                    @else
                    Aucun frais de dossier
                    @endif
                </table>
            </td>
            <td class="bold">{{ number_format(($project->totalProjectFeesHT + $project->totalCommissionHT)/100,2) }}€
            </td>
            <td class="bold">{{ number_format(($project->totalProjectFeesHT + $project->totalCommissionHT +
                $project->totalEngagedFeesHT)/100,2) }}€
            </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="2"></td>
            <td class="small">Total F.Engagés (HT)</td>
            <td class=" bold">{{ number_format(array_reduce($projects->all(), function($carry,$item){
                $carry += (($item->totalEngagedFeesHT)/100);
                return $carry;
                },0),2) }}€</td>
            <td class="small">Total F.Dossier (HT)</td>
            <td class=" bold">{{ number_format(array_reduce($projects->all(), function($carry,$item){
                $carry += (($item->totalProjectFeesHT + $item->totalCommissionHT)/100);
                return $carry;
                },0),2) }}€</td>

            <td class="primary right bold ">
                {{ number_format(array_reduce($projects->all(), function($carry,$item){
                $carry += (($item->totalProjectFeesHT + $item->totalCommissionHT +
                $item->totalEngagedFeesHT)/100);
                return $carry;
                },0),2) }}€
            </td>
        </tr>
        <tr>
            <td colspan="5">
            </td>
            <td class="bold">
                Total <span class="small">(HT)</span>
            </td>
            <td class="bold">{{ number_format(array_reduce($projects->all(), function($carry,$item){
                $carry += (($item->totalProjectFeesHT + $item->totalCommissionHT +
                $item->totalEngagedFeesHT)/100);
                return $carry;
                },0),2) }}€</td>
        </tr>
        <tr>
            <td colspan="5">
            </td>
            <td class="bold">
                Total <span class="small">(TTC)</span>
            </td>
            <td class="bold primary">{{ number_format(array_reduce($projects->all(), function($carry,$item){
                $carry += (($item->totalProjectFeesHT + $item->totalCommissionHT +
                $item->totalEngagedFeesHT)/100);
                return $carry;
                },0)*1.2,2) }}€</td>
        </tr>
</table>
@endif

@endsection