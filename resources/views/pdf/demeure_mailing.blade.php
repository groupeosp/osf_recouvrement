@extends('layouts.account-mail',['user' => $debtor,'ndoss'=>$id,'nref'=>'1er Rappel','object'=>'Mise en demeure de
payer.'])

@section('content')
<header>
    <p>Malgré nos différentes relance, par lesquelles nous vous réclamions le paiement de(s) facture(s) d'un montant de
        <b>{{ number_format($creance,2) }} €</b> TTC. Nous sommes surpris de n'avoir reçu aucun règlement de votre part.
        </v>
    </p>
</header>
<main>
    @if ($billFiles)
    <table>
        <thead>
            <th>Date</th>
            <th>Facture</th>
            <th>Affaire</th>
            <th>Montant</th>
            <th>Payé</th>
            <th>Reste à payer</th>
        </thead>
        <tbody>
            @foreach ($billFiles as $file)
            <tr>
                <td>{{ $file->bill_date }}</td>
                <td>{{ strtoupper($file->bill_id) }}</td>
                <td>{{ $file->bill_title }}</td>
                <td class="right">{{ number_format($file->bill_amount_ttc / 100,2) }}€</td>
                <td class="right">{{ number_format($file->bill_left/100,2) }}€</td>
                <td class="right">{{ number_format(($file->bill_amount_ttc - $file->bill_left)/100,2) }}€</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="5" class="right bold">Montant du principal :</td>
                <td class="right">
                    <b>{{ number_format(array_reduce($billFiles, function($carry,$item){
                        $carry += (($item->bill_amount_ttc - $item->bill_left)/100);
                        return $carry;
                        }),2) }}€</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" class="right bold">Indémnité forfaitaire fais de recouvrement Art L441-6 :</td>
                <td class="right">
                    <b>{{ number_format(80,2) }}€</b>
                </td>
            </tr>
            <tr>
                <td colspan="5" class="right bold">Total dû :</td>
                <td class="right">
                    <b>{{ number_format(array_reduce($billFiles, function($carry,$item){
                        $carry += (($item->bill_amount_ttc - $item->bill_left)/100);
                        return $carry;
                        })+80,2) }}€</b>
                </td>
            </tr>
    </table>
    @endif

</main>
<footer>
    <p><b>Nous de sommes pas opposé à un règlement amiable de notre différend et nous nous tenons à votre disposition
            pour en discuter. A défaut de réponse sous huit jours, nous estimerons que vous refusez toute solution
            amiable.</b></p>
    <br>
    <p><b>En tout état de cause, vous devez considérer la présente comme une mise en demeure, de nature à faire courir
            tous délais, intérêts et autres conséquences que la Loi - particulièrement l'article 1153 du code civil - et
            les Tribunaux attachent aux mises en demeure.
        </b></p>
</footer>
@endsection