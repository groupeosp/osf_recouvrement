@extends('layouts.account-mail',['user' => $debtor,'ndoss'=>$id,'nref'=>'1er Rappel','object'=>'Relance de facture(s)
impayée(s).'])

@section('content')
<header>
    <p>Nous nous permettons de vous signaler que, sauf erreur ou omission de notre part, nous ne
        trouvons pas trace du règlement de(s) facture(s) échue(s) à ce jour : <b>{{ number_format($creance,2) }} €</b>.
        </v>
    </p>
</header>
<main>
    @if ($billFiles)
    <table>
        <thead>
            <th>Date</th>
            <th>Facture</th>
            <th>Affaire</th>
            <th>Montant</th>
            <th>Payé</th>
            <th>Reste à payer</th>
        </thead>
        <tbody>
            @foreach ($billFiles as $file)
            <tr>
                <td>{{ $file->bill_date }}</td>
                <td>{{ strtoupper($file->bill_id) }}</td>
                <td>{{ $file->bill_title }}</td>
                <td class="right">{{ number_format($file->bill_amount_ttc / 100,2) }}€</td>
                <td class="right">{{ number_format($file->bill_left/100,2) }}€</td>
                <td class="right">{{ number_format(($file->bill_amount_ttc - $file->bill_left)/100,2) }}€</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="5" class="right total">Total à payer :</td>
                <td class="right">
                    <b>{{ number_format(array_reduce($billFiles, function($carry,$item){
                        $carry += (($item->bill_amount_ttc - $item->bill_left)/100);
                        return $carry;
                        }),2) }}€</b>
                </td>
            </tr>
    </table>
    @endif

</main>
<footer>
    <p><b>La loi LME du 04 août 2008 stipule que le délai convenu entre les parties pour régler les
            sommes
            dues, ne peut dépasser 45 jours fin de mois ou 60 jours à compter de la date d'émission
            de
            la
            facture.</b></p>
    <br>
    <p><b>Corformément à l'aricle L.441-10 du code de commerce, des pénalités de retard sont dues à
            défaut
            de paiement de facture à son échéance.<br>
            De plus, une indemnité forfaitaire de frais de recouvrement de 40.00€ est due de plein
            droit
            en
            cas de retard de paiement.
        </b></p>
    <br>
    <p>
        Nous vous remercions de bien vouloir vérifier dans votre comptabilité et de nous faire
        parvenir
        le règlement de celle(s)-ci par retour de courrier, sauf à nous informer des raisons qui
        s'opposent à ce paiement.
    </p>
</footer>
@endsection