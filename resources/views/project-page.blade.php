@extends('layouts.app')

@section('content')

<project-page-component :project_id="{{ request()->project_id }}" user_role="{{ Auth::user()->role->value }}">
</project-page-component>
@endsection