<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\FeeController;
use App\Http\Controllers\BillController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\StepController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DebtorController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\ClientBillController;
use App\Http\Controllers\ProjectStepController;
use App\Http\Controllers\PDFController\SimpleMailingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware([auth::class])->group(function () {


    //Ensure we do not display all the projects list to non-admin users
    Route::middleware(['client.user', 'add.client_id'])->group(function () {
        Route::get('/', function () {
            return redirect(route('home'));
        });
        Route::view('/dossiers', 'home')->name('home');
        Route::view('/dossiers-factures', 'closed-projects')->name('projects-page.closed');
        Route::view('/dossiers/{project_id}', 'project-page')->whereNumber('project_id')->name('project');
        Route::view('/dossiers/nouveau', 'new-project')->name('new-project');

        Route::view('/encaissements', 'encaissements')->name('encaissements');

        Route::view('/client', 'client')->name('client');

        Route::controller(SimpleMailingController::class)->group(function () {
            Route::get('/dossiers/{project_id}/{project_step_id}/courier', 'generatePDF');
        });
    });

    // Change the cookie client_id to the current user client_id
    Route::post('/clients/change', function (Request $request) {
        Cookie::queue('client_id',  $request->client_id, 60);
        return redirect()->back();
    })->name('client.change');

    Route::prefix('api')->group(function () {
        /**
         * Clients
         */
        Route::controller(ClientController::class)->group(function () {
            Route::get('/clients', 'index')->name('clients.index');
            Route::get('/clients/{client_id}', 'show')->name('clients.show');
            Route::patch('/clients/{client_id}', 'update')->name('clients.update');
        });

        /**
         * Projects
         */
        Route::controller(ProjectController::class)->group(function () {
            Route::post('/projects', 'store')->name('projects.Store');
            Route::patch('/projects/{project_id}', 'update')->name('projects.update');
            Route::post('/projects/all', 'index')->name('projects.index');
            Route::post('/projects/export', 'export')->name('projects.export');
            Route::get('/projects/{project_id}', 'show')->name('projects.show');
            Route::delete('/projects/{project_id}', 'destroy')->name('projects.destroy');
        });

        /**
         * Contracts
         */
        Route::controller(ContractController::class)->group(function () {
            Route::post('/contracts', 'store')->name('contracts.store');
            Route::get('/contracts/{contract_id}', 'show')->name('contracts.show');
            Route::delete('/contracts/{contract_id}', 'destroy')->name('contracts.destroy')->middleware('isAtOSP&OSPUser');
            Route::get('/contracts/{contract_id}/download', 'download')->name('contracts.download');
        });

        /**
         * Project_step
         */
        Route::controller(ProjectStepController::class)->group(function () {
            Route::put('/project_steps/{project_id}/{step_id}', 'update')->name('project_steps.update');
            Route::post('/project_steps/{project_id}/{step_id}', 'store')->name('project_steps.store');
            Route::delete('/project_steps/{project_id}/{step_id}', 'destroy')->name('project_steps.destroy');
        });

        /**
         * Files
         */
        Route::controller(FileController::class)->group(function () {
            Route::get('/files', 'index')->name('files.index');
            Route::post('/files', 'store')->name('files.store');
            Route::patch('/files/{file_id}', 'update')->name('files.update');
            Route::get('/files/{file_id}', 'show')->name('files.show');
            Route::delete('/files/{file_id}', 'destroy')->name('files.destroy')->middleware('isAtOSP&OSPUser');
        });

        /**
         * Steps
         */
        Route::controller(StepController::class)->group(function () {
            Route::get('/steps', 'index')->name('steps.index');
        });
        /**
         * Bills
         */
        Route::controller(BillController::class)->group(function () {
            Route::middleware(['trusted'])->group(function () {
                Route::get('/bills', 'index')->name('bills.index');
                Route::patch('/bills', 'store')->name('bills.store');
                Route::delete('/bills/{bill_id}', 'destroy')->name('bills.destroy');
                Route::post('/bills/{bill_id}/validate', 'validation')->name('bills.validate');
            });
        });
        /**
         * Notes
         */
        Route::controller(NoteController::class)->group(function () {
            Route::middleware(['trusted'])->group(function () {
                Route::get('/notes', 'index')->name('notes.index');
                Route::post('/notes', 'store')->name('notes.store');
                Route::patch('/notes/{note_id}', 'update')->name('notes.update');
                Route::delete('/notes/{note_id}', 'destroy')->name('notes.destroy');
            });
        });
        /**
         * Fees
         */
        Route::controller(FeeController::class)->group(function () {

            Route::middleware(['trusted'])->group(function () {
                Route::get('/fees', 'index')->name('fees.index');
                Route::patch('/fees', 'store')->name('fees.store');
                Route::delete('/fees/{fee_id}', 'destroy')->name('fees.destroy');
            });
        });

        /**
         * Debtors
         */
        Route::controller(DebtorController::class)->group(function () {
            Route::middleware(['trusted'])->group(function () {
                Route::get('/debtors', 'index')->name('debtors.index');
                Route::post('/debtors', 'store')->name('debtors.store');
                Route::delete('/debtors/{debtor_id}', 'destroy')->name('debtors.destroy');
            });
        });

        /**
         * Clients Bills
         */
        Route::controller(ClientBillController::class)->group(function () {
            Route::middleware(['trusted'])->group(function () {
                Route::get('/client_bills/preview', 'previewPDF')->name('client_bills.previewPDF');
                Route::get('/client_bills/export', 'exportPDF')->name('client_bills.exportPDF');
                Route::get('/client_bills/{client_bills_id}', 'show')->name('client_bills.show');
                Route::get('/client_bills/{client_bills_id}/download', 'download')->name('client_bills.download');
                Route::delete('/client_bills/{client_bills_id}', 'destroy')->name('client_bills.destroy');
            });
        });
    });
});

Auth::routes();
