module.exports = {
   content: ["./storage/framework/views/*.php", "./resources/**/*.blade.php", "./resources/**/*.js", "./resources/**/*.vue"],
   darkMode: true, // or 'media' or 'class'
   theme: {
      extend: {
         inset: {
            100: "10rem",
         },
         colors: {
            myblack: "#1a3463",
            myprimary: "#286BEE",
            mygrey: "#FEFEFE",
            mywhite: "#FFFFFF",
            mygrey2: "#E7ECEE",
            mygrey3: "#F6F6F6",
            mydarkgrey: "#7A868C",
            myred: "#eb4f4f",
            mygreen: "#0acf83",
            myblue: "#3069AC",

            mylightyellow: "#FBF7D7",
            mylightred: "#fff4f4",
            mylightgreen: "#ebfdeb",
            mylightblue: "#EAEFF9",
            mylightpink: "#FFE9FF",
         },
         boxShadow: {
            border: "inset 0 0 2px #7A868C",
         },
         width: {
            1800: "100rem",
            1500: "70rem",
            900: "50rem",
            800: "48rem",
            750: "45rem",
            700: "42rem",
            650: "40rem",
            600: "38rem",
            500: "32rem",
            450: "29rem",
            400: "27rem",
            300: "20rem",
         },
         height: {
            800: "48rem",
            700: "42rem",
            650: "41rem",
            600: "38rem",
            550: "36rem",
            500: "30rem",
            400: "27rem",
            300: "20rem",
            100: "10rem",
         },
         maxHeight: {
            400: "27rem",
         },
      },
   },
   variants: {
      extend: {},
   },
   plugins: [
      require("@tailwindcss/line-clamp"),
      // ...
   ],
};
